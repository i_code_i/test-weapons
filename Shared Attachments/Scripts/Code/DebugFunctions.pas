.info
  .source "F:\\Mod Stuff\\Fallout 4\\MO2\\mods\\Code Shared Attachments\\Scripts\\Source\\User\\Code\\DebugFunctions.psc"
  .modifyTime 1646794384
  .compileTime 1651164048
  .user "Sam"
  .computer "SAMS_DESKTOP"
.endInfo
.userFlagsRef
  .flag default 2
  .flag mandatory 5
  .flag collapsedonbase 4
  .flag hidden 0
  .flag collapsedonref 3
  .flag conditional 1
.endUserFlagsRef
.objectTable
  .object Code:DebugFunctions ScriptObject 
    .userFlags 1
    .docString ""
    .autoState 
    .structTable
    .endStructTable
    .variableTable
    .endVariableTable
    .propertyTable
    .endPropertyTable
    .propertyGroupTable
    .endPropertyGroupTable
    .stateTable
      .state
        .function GetOMODsAtSlotIndex static
          .userFlags 0
          .docString ""
          .return bool
          .paramTable
            .param aSlotIndex int
          .endParamTable
          .localTable
            .local ::temp0 actor
            .local objectMods objectmod[]
            .local ::temp2 int
            .local ::temp3 bool
            .local index int
            .local ::temp4 objectmod
            .local ::temp5 string
            .local ::temp6 string
            .local ::temp7 string
            .local ::temp8 string
            .local ::temp9 string
            .local ::temp10 bool
            .local ::temp11 bool
            .local ::temp12 objectmod
            .local ::temp13 string
            .local ::temp14 string
            .local ::temp15 string
            .local ::temp16 string
            .local ::temp17 string
          .endLocalTable
          .code
            CALLSTATIC game GetPlayer ::temp0  ;@line 10
            CALLMETHOD GetWornItemMods ::temp0 objectMods aSlotIndex ;@line 10
            JUMPF objectMods label7 ;@line 11
            ASSIGN index 0 ;@line 12
            label1:
            ARRAYLENGTH ::temp2 objectMods ;@line 13
            COMPARELT ::temp3 index ::temp2 ;@line 13
            JUMPF ::temp3 label2 ;@line 13
            ARRAYGETELEMENT ::temp4 objectMods index ;@line 14
            CAST ::temp5 ::temp4 ;@line 14
            STRCAT ::temp6 "Found the object mod '" ::temp5 ;@line 14
            STRCAT ::temp7 ::temp6 "' on slot index " ;@line 14
            CAST ::temp8 aSlotIndex ;@line 14
            STRCAT ::temp9 ::temp7 ::temp8 ;@line 14
            CALLSTATIC debug TraceUser ::temp10 "Code_Debug" ::temp9 0 ;@line 14
            JUMPF ::temp10 label4 ;@line 14
            JUMP label3
            label4:
            CALLSTATIC debug OpenUserLog ::temp11 "Code_Debug" ;@line 18
            ARRAYGETELEMENT ::temp12 objectMods index ;@line 19
            CAST ::temp13 ::temp12 ;@line 19
            STRCAT ::temp14 "Found the object mod '" ::temp13 ;@line 19
            STRCAT ::temp15 ::temp14 "' on slot index " ;@line 19
            CAST ::temp16 aSlotIndex ;@line 19
            STRCAT ::temp17 ::temp15 ::temp16 ;@line 19
            CALLSTATIC debug TraceUser ::temp11 "Code_Debug" ::temp17 0 ;@line 19
            label3:
            IADD index index 1 ;@line 21

            JUMP label1
            label2:
            RETURN True ;@line 23
            JUMP label0
            label7:
            CAST ::temp13 aSlotIndex ;@line 25
            STRCAT ::temp14 "Slot index " ::temp13 ;@line 25
            STRCAT ::temp15 ::temp14 " had no object mods." ;@line 25
            CALLSTATIC debug TraceUser ::temp11 "Code_Debug" ::temp15 0 ;@line 25
            JUMPF ::temp11 label6 ;@line 25
            JUMP label5
            label6:
            CALLSTATIC debug OpenUserLog ::temp10 "Code_Debug" ;@line 29
            CAST ::temp16 aSlotIndex ;@line 30
            STRCAT ::temp17 "Slot index " ::temp16 ;@line 30
            STRCAT ::temp5 ::temp17 " had no object mods." ;@line 30
            CALLSTATIC debug TraceUser ::temp3 "Code_Debug" ::temp5 0 ;@line 30
            label5:
            RETURN False ;@line 32
            label0:
          .endCode
        .endFunction
        .function GetAllWornItemInfo static
          .userFlags 0
          .docString ""
          .return NONE
          .paramTable
          .endParamTable
          .localTable
            .local ::temp19 bool
            .local index int
            .local end int
            .local ::temp20 actor
            .local ::temp22 string
            .local ::temp23 string
            .local ::temp24 string
            .local ::temp25 string
            .local ::temp26 string
            .local ::temp27 bool
            .local wornItem actor#wornitem
            .local ::temp28 bool
            .local ::temp29 string
            .local ::temp30 string
            .local ::temp31 string
            .local ::temp32 string
            .local ::temp33 string
          .endLocalTable
          .code
            ASSIGN index 0 ;@line 38
            ASSIGN end 43 ;@line 39
            label8:
            COMPARELT ::temp19 index end ;@line 41
            JUMPF ::temp19 label9 ;@line 41
            CALLSTATIC game GetPlayer ::temp20  ;@line 42
            CALLMETHOD GetWornItem ::temp20 wornItem index false ;@line 42
            CAST ::temp22 index ;@line 43
            STRCAT ::temp23 "Slot Index: " ::temp22 ;@line 43
            STRCAT ::temp24 ::temp23 ", " ;@line 43
            CAST ::temp25 wornItem ;@line 43
            STRCAT ::temp26 ::temp24 ::temp25 ;@line 43
            CALLSTATIC debug TraceUser ::temp27 "Code_Debug" ::temp26 0 ;@line 43
            JUMPF ::temp27 label11 ;@line 43
            JUMP label10
            label11:
            CALLSTATIC debug OpenUserLog ::temp28 "Code_Debug" ;@line 47
            CAST ::temp29 index ;@line 48
            STRCAT ::temp30 "Slot Index: " ::temp29 ;@line 48
            STRCAT ::temp31 ::temp30 ", " ;@line 48
            CAST ::temp32 wornItem ;@line 48
            STRCAT ::temp33 ::temp31 ::temp32 ;@line 48
            CALLSTATIC debug TraceUser ::temp28 "Code_Debug" ::temp33 0 ;@line 48
            label10:
            CALLSTATIC code:debugfunctions GetOMODsAtSlotIndex ::temp28 index ;@line 50
            IADD index index 1 ;@line 51

            JUMP label8
            label9:
          .endCode
        .endFunction
      .endState
    .endStateTable
  .endObject
.endObjectTable