.info
  .source "F:\\Mod Stuff\\Fallout 4\\MO2\\mods\\Code Shared Attachments\\Scripts\\Source\\User\\Code\\AliasThermalScopeScript.psc"
  .modifyTime 1646966895
  .compileTime 1651164048
  .user "Sam"
  .computer "SAMS_DESKTOP"
.endInfo
.userFlagsRef
  .flag default 2
  .flag mandatory 5
  .flag collapsedonbase 4
  .flag hidden 0
  .flag collapsedonref 3
  .flag conditional 1
.endUserFlagsRef
.objectTable
  .object Code:AliasThermalScopeScript ReferenceAlias 
    .userFlags 0
    .docString ""
    .autoState 
    .structTable
    .endStructTable
    .variableTable
      .variable ::ThermalScopeSpell_var spell 
        .userFlags 0
        .initialValue None
      .endVariable
      .variable ::HasScopeThermal_var keyword 
        .userFlags 0
        .initialValue None
      .endVariable
      .variable ::ThermalScopeEquippedEffect_var magiceffect 
        .userFlags 0
        .initialValue None
      .endVariable
      .variable ThermalTick float 
        .userFlags 0
        .initialValue 0.25
      .endVariable
      .variable ThermalTimerId int 
        .userFlags 0
        .initialValue 97097
      .endVariable
      .variable Log code:log#userlog 
        .userFlags 0
        .initialValue None
      .endVariable
    .endVariableTable
    .propertyTable
	  .property Player actor
	    .userFlags 1
	    .docString ""
	    .function Get 
	      .userFlags 0
	      .docString ""
	      .return actor
	      .paramTable
	      .endParamTable
	      .localTable
	        .local ::temp0 actor
	      .endLocalTable
	      .code
	        CALLSTATIC game GetPlayer ::temp0  ;@line 9
	        RETURN ::temp0 ;@line 9
	      .endCode
	    .endFunction
	  .endProperty
	  .property ThermalScopeSpell spell auto
	    .userFlags 0
	    .docString ""
	    .autoVar ::ThermalScopeSpell_var
	  .endProperty
	  .property HasScopeThermal keyword auto
	    .userFlags 0
	    .docString ""
	    .autoVar ::HasScopeThermal_var
	  .endProperty
	  .property ThermalScopeEquippedEffect magiceffect auto
	    .userFlags 0
	    .docString ""
	    .autoVar ::ThermalScopeEquippedEffect_var
	  .endProperty
    .endPropertyTable
    .propertyGroupTable
      .propertyGroup Properties
        .userFlags 0
        .docString ""
        .property Player
        .property ThermalScopeSpell
        .property HasScopeThermal
        .property ThermalScopeEquippedEffect
      .endPropertyGroup
    .endPropertyGroupTable
    .stateTable
      .state
        .function OnAliasInit 
          .userFlags 0
          .docString ""
          .return NONE
          .paramTable
          .endParamTable
          .localTable
            .local ::temp2 string
          .endLocalTable
          .code
            STRUCTCREATE Log ;@line 22

            CAST ::temp2 self ;@line 23
            STRUCTSET Log Caller ::temp2 ;@line 23
            ASSIGN ::temp2 "Code_ThermalScope" ;@line 24
            STRUCTSET Log FileName ::temp2 ;@line 24
          .endCode
        .endFunction
        .function OnItemEquipped 
          .userFlags 0
          .docString ""
          .return NONE
          .paramTable
            .param akBaseObject form
            .param akReference objectreference
          .endParamTable
          .localTable
            .local ::temp4 scriptobject
            .local ::temp6 string
            .local ::temp8 actor
            .local ::temp9 weapon
            .local CurrentWeapon form
            .local CurrentWeaponName String
            .local ::temp10 actor
            .local ::temp11 weapon
            .local ::temp13 string
            .local ::temp14 string
            .local ::temp15 string
            .local ::temp16 string
            .local ::temp17 var
            .local ::temp18 bool
            .local ::temp19 bool
            .local ::nonevar none
          .endLocalTable
          .code
            CAST ::temp4 self ;@line 28
            CALLSTATIC code:log GetLog Log ::temp4 ;@line 28

            ASSIGN ::temp6 "Code_ThermalScope" ;@line 29
            STRUCTSET Log FileName ::temp6 ;@line 29
            ASSIGN CurrentWeapon none ;@line 31
            ASSIGN CurrentWeaponName "" ;@line 32
            PROPGET Player self ::temp8 ;@line 33
            CALLMETHOD GetEquippedWeapon ::temp8 ::temp9 0 ;@line 33
            JUMPF ::temp9 label1 ;@line 33
            PROPGET Player self ::temp10 ;@line 34
            CALLMETHOD GetEquippedWeapon ::temp10 ::temp11 0 ;@line 34
            CAST CurrentWeapon ::temp11 ;@line 34

            CALLMETHOD GetName CurrentWeapon CurrentWeaponName  ;@line 35

            CAST ::temp13 CurrentWeapon ;@line 36
            STRCAT ::temp14 "Equiping " ::temp13 ;@line 36
            STRCAT ::temp15 ::temp14 " AKA: " ;@line 36
            STRCAT ::temp16 ::temp15 CurrentWeaponName ;@line 36
            CAST ::temp17 ::temp16 ;@line 36
            CALLSTATIC code:log WriteLine ::temp18 Log ::temp17 0 ;@line 36
            JUMP label0
            label1:
            label0:
            PROPGET Player self ::temp10 ;@line 39
            CALLMETHOD HasMagicEffect ::temp10 ::temp18 ::ThermalScopeEquippedEffect_var ;@line 39
            JUMPF ::temp18 label3 ;@line 39
            CAST ::temp17 " Thermal Scope weapon detected, starting timer" ;@line 40
            CALLSTATIC code:log WriteLine ::temp19 Log ::temp17 0 ;@line 40
            CALLMETHOD StartTimer self ::nonevar ThermalTick ThermalTimerId ;@line 42
            JUMP label2
            label3:
            label2:
          .endCode
        .endFunction
        .function OnTimer 
          .userFlags 0
          .docString ""
          .return NONE
          .paramTable
            .param aiTimerID Int
          .endParamTable
          .localTable
            .local ::temp20 string
            .local ::temp21 string
            .local ::temp22 var
            .local ::temp23 bool
            .local ::temp24 actor
            .local ::temp25 bool
            .local ::temp26 actor
            .local ::temp27 bool
            .local ::temp28 bool
            .local ::temp29 bool
            .local ::temp30 actor
            .local ::temp31 actor
            .local ::temp32 objectreference
            .local ::nonevar none
          .endLocalTable
          .code
            CAST ::temp20 aiTimerID ;@line 47
            STRCAT ::temp21 " aiTimerID: " ::temp20 ;@line 47
            CAST ::temp22 ::temp21 ;@line 47
            CALLSTATIC code:log WriteLine ::temp23 Log ::temp22 0 ;@line 47
            PROPGET Player self ::temp24 ;@line 50
            CALLMETHOD HasMagicEffect ::temp24 ::temp23 ::ThermalScopeEquippedEffect_var ;@line 50
            JUMPF ::temp23 label8 ;@line 50
            COMPAREEQ ::temp25 aiTimerID ThermalTimerId ;@line 51
            CAST ::temp28 ::temp25 ;@line 51
            JUMPF ::temp28 label6 ;@line 51
            PROPGET Player self ::temp26 ;@line 51
            CALLMETHOD IsInIronSights ::temp26 ::temp27  ;@line 51
            CAST ::temp28 ::temp27 ;@line 51
            label6:
            JUMPF ::temp28 label7 ;@line 51
            CAST ::temp22 " casting Targeting spell" ;@line 53
            CALLSTATIC code:log WriteLine ::temp29 Log ::temp22 0 ;@line 53
            PROPGET Player self ::temp30 ;@line 54
            CAST ::temp32 ::temp30 ;@line 54
            PROPGET Player self ::temp31 ;@line 54
            CALLMETHOD RemoteCast ::ThermalScopeSpell_var ::nonevar ::temp32 ::temp31 NONE ;@line 54
            JUMP label5
            label7:
            label5:
            CALLMETHOD StartTimer self ::nonevar ThermalTick ThermalTimerId ;@line 57
            JUMP label4
            label8:
            label4:
          .endCode
        .endFunction
        .function OnItemUnequipped 
          .userFlags 0
          .docString ""
          .return NONE
          .paramTable
            .param akBaseObject form
            .param akReference objectreference
          .endParamTable
          .localTable
            .local ::temp33 var
            .local ::temp34 bool
            .local ::nonevar none
          .endLocalTable
          .code
            CAST ::temp33 " canceling Targeting timer" ;@line 64
            CALLSTATIC code:log WriteLine ::temp34 Log ::temp33 0 ;@line 64
            CALLMETHOD CancelTimer self ::nonevar ThermalTimerId ;@line 66
          .endCode
        .endFunction
      .endState
    .endStateTable
  .endObject
.endObjectTable