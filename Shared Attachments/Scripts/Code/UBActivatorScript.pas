.info
  .source "F:\\Mod Stuff\\Fallout 4\\MO2\\mods\\Code Shared Attachments\\Scripts\\Source\\User\\Code\\UBActivatorScript.psc"
  .modifyTime 1646793069
  .compileTime 1651164048
  .user "Sam"
  .computer "SAMS_DESKTOP"
.endInfo
.userFlagsRef
  .flag default 2
  .flag mandatory 5
  .flag collapsedonbase 4
  .flag hidden 0
  .flag collapsedonref 3
  .flag conditional 1
.endUserFlagsRef
.objectTable
  .object Code:UBActivatorScript activemagiceffect 
    .userFlags 0
    .docString ""
    .autoState 
    .structTable
    .endStructTable
    .variableTable
      .variable ::ActivatorItem_var form const
        .userFlags 0
        .initialValue None
      .endVariable
      .variable ::MasterKey_var objectmod 
        .userFlags 0
        .initialValue None
      .endVariable
      .variable ::GrenadeLauncher_var objectmod 
        .userFlags 0
        .initialValue None
      .endVariable
      .variable ::ActionCustomGrenadeLauncherTo_var action const
        .userFlags 0
        .initialValue None
      .endVariable
      .variable ::ActionCustomGrenadeLauncherFrom_var action const
        .userFlags 0
        .initialValue None
      .endVariable
      .variable ::ActionCustomMasterKeyTo_var action const
        .userFlags 0
        .initialValue None
      .endVariable
      .variable ::ActionCustomMasterKeyFrom_var action const
        .userFlags 0
        .initialValue None
      .endVariable
      .variable ::AnimsActive_var keyword const
        .userFlags 0
        .initialValue None
      .endVariable
      .variable ::AnimsGL_var keyword 
        .userFlags 0
        .initialValue None
      .endVariable
      .variable ::AnimsMK01_var keyword 
        .userFlags 0
        .initialValue None
      .endVariable
      .variable ::IsGrenadeLauncher_var actorvalue 
        .userFlags 0
        .initialValue None
      .endVariable
      .variable ::IsMasterKey_var actorvalue 
        .userFlags 0
        .initialValue None
      .endVariable
      .variable ::IsAttaching_var actorvalue 
        .userFlags 0
        .initialValue None
      .endVariable
      .variable ::SpecialEquip_var actorvalue 
        .userFlags 0
        .initialValue None
      .endVariable
      .variable Log code:log#userlog 
        .userFlags 0
        .initialValue None
      .endVariable
    .endVariableTable
    .propertyTable
	  .property ActivatorItem form auto
	    .userFlags 0
	    .docString ""
	    .autoVar ::ActivatorItem_var
	  .endProperty
	  .property MasterKey objectmod auto
	    .userFlags 0
	    .docString ""
	    .autoVar ::MasterKey_var
	  .endProperty
	  .property GrenadeLauncher objectmod auto
	    .userFlags 0
	    .docString ""
	    .autoVar ::GrenadeLauncher_var
	  .endProperty
	  .property ActionCustomGrenadeLauncherTo action auto
	    .userFlags 0
	    .docString ""
	    .autoVar ::ActionCustomGrenadeLauncherTo_var
	  .endProperty
	  .property ActionCustomGrenadeLauncherFrom action auto
	    .userFlags 0
	    .docString ""
	    .autoVar ::ActionCustomGrenadeLauncherFrom_var
	  .endProperty
	  .property ActionCustomMasterKeyTo action auto
	    .userFlags 0
	    .docString ""
	    .autoVar ::ActionCustomMasterKeyTo_var
	  .endProperty
	  .property ActionCustomMasterKeyFrom action auto
	    .userFlags 0
	    .docString ""
	    .autoVar ::ActionCustomMasterKeyFrom_var
	  .endProperty
	  .property AnimsActive keyword auto
	    .userFlags 0
	    .docString ""
	    .autoVar ::AnimsActive_var
	  .endProperty
	  .property AnimsGL keyword auto
	    .userFlags 0
	    .docString ""
	    .autoVar ::AnimsGL_var
	  .endProperty
	  .property AnimsMK01 keyword auto
	    .userFlags 0
	    .docString ""
	    .autoVar ::AnimsMK01_var
	  .endProperty
	  .property IsGrenadeLauncher actorvalue auto
	    .userFlags 0
	    .docString ""
	    .autoVar ::IsGrenadeLauncher_var
	  .endProperty
	  .property IsMasterKey actorvalue auto
	    .userFlags 0
	    .docString ""
	    .autoVar ::IsMasterKey_var
	  .endProperty
	  .property IsAttaching actorvalue auto
	    .userFlags 0
	    .docString ""
	    .autoVar ::IsAttaching_var
	  .endProperty
	  .property SpecialEquip actorvalue auto
	    .userFlags 0
	    .docString ""
	    .autoVar ::SpecialEquip_var
	  .endProperty
    .endPropertyTable
    .propertyGroupTable
      .propertyGroup Properties
        .userFlags 0
        .docString ""
        .property ActivatorItem
        .property MasterKey
        .property GrenadeLauncher
        .property ActionCustomGrenadeLauncherTo
        .property ActionCustomGrenadeLauncherFrom
        .property ActionCustomMasterKeyTo
        .property ActionCustomMasterKeyFrom
        .property AnimsActive
        .property AnimsGL
        .property AnimsMK01
      .endPropertyGroup
      .propertyGroup Vars
        .userFlags 0
        .docString ""
        .property IsGrenadeLauncher
        .property IsMasterKey
        .property IsAttaching
        .property SpecialEquip
      .endPropertyGroup
    .endPropertyGroupTable
    .stateTable
      .state
        .function OnEffectStart 
          .userFlags 0
          .docString ""
          .return NONE
          .paramTable
            .param akTarget actor
            .param akCaster actor
          .endParamTable
          .localTable
            .local ::temp0 scriptobject
            .local ::temp2 string
            .local ::temp3 weapon
            .local ::temp5 bool
            .local CurrentWeapon form
            .local CurrentWeaponName String
            .local ::temp6 bool
            .local ::temp7 bool
            .local ::temp8 string
            .local ::temp9 string
            .local ::temp10 string
            .local ::temp11 string
            .local ::temp12 string
            .local ::temp13 string
            .local ::temp14 var
            .local ::temp15 bool
            .local ::nonevar none
            .local ::temp16 objectreference
            .local ::temp17 bool
            .local ::temp18 var
            .local ::temp19 bool
            .local ::temp20 string
            .local ::temp21 string
            .local ::temp22 string
            .local ::temp23 string
            .local ::temp24 string
            .local ::temp25 string
            .local ::temp26 bool
            .local ::temp27 objectreference
            .local ::temp28 var
            .local ::temp29 bool
            .local ::temp30 var
          .endLocalTable
          .code
            CAST ::temp0 self ;@line 29
            CALLSTATIC code:log GetLog Log ::temp0 ;@line 29

            ASSIGN ::temp2 "Code_UnderBarrel" ;@line 30
            STRUCTSET Log FileName ::temp2 ;@line 30
            CALLMETHOD GetEquippedWeapon akTarget ::temp3 0 ;@line 31
            CAST CurrentWeapon ::temp3 ;@line 31
            CALLMETHOD GetName CurrentWeapon CurrentWeaponName  ;@line 32
            CALLMETHOD WornHasKeyword akTarget ::temp5 ::AnimsGL_var ;@line 33
            JUMPF ::temp5 label8 ;@line 33
            CALLMETHOD WornHasKeyword akTarget ::temp6 ::AnimsActive_var ;@line 35
            NOT ::temp7 ::temp6 ;@line 35
            JUMPF ::temp7 label3 ;@line 35
            CAST ::temp8 akTarget ;@line 37
            STRCAT ::temp9 ::temp8 " Grenade Launcher is now active on their " ;@line 37
            CAST ::temp10 CurrentWeapon ;@line 37
            STRCAT ::temp11 ::temp9 ::temp10 ;@line 37
            STRCAT ::temp12 ::temp11 " AKA: " ;@line 37
            STRCAT ::temp13 ::temp12 CurrentWeaponName ;@line 37
            CAST ::temp14 ::temp13 ;@line 37
            CALLSTATIC code:log WriteLine ::temp15 Log ::temp14 0 ;@line 37
            CALLMETHOD AddItem akTarget ::nonevar ::ActivatorItem_var 1 True ;@line 38
            CALLMETHOD AttachModToInventoryItem akTarget ::temp15 CurrentWeapon ::GrenadeLauncher_var ;@line 41
            CAST ::temp16 akTarget ;@line 46
            CALLMETHOD WaitFor3DLoad ::temp16 ::temp15  ;@line 46
            CALLMETHOD PlayIdleAction akTarget ::temp15 ::ActionCustomGrenadeLauncherTo_var None ;@line 48
            JUMP label1
            label3:
            CALLMETHOD WornHasKeyword akTarget ::temp15 ::AnimsActive_var ;@line 51
            JUMPF ::temp15 label2 ;@line 51
            CAST ::temp8 akTarget ;@line 53
            STRCAT ::temp9 ::temp8 " Grenade Launcher is now inactive on their " ;@line 53
            CAST ::temp10 CurrentWeapon ;@line 53
            STRCAT ::temp11 ::temp9 ::temp10 ;@line 53
            STRCAT ::temp12 ::temp11 " AKA: " ;@line 53
            STRCAT ::temp13 ::temp12 CurrentWeaponName ;@line 53
            CAST ::temp14 ::temp13 ;@line 53
            CALLSTATIC code:log WriteLine ::temp17 Log ::temp14 0 ;@line 53
            CALLMETHOD AddItem akTarget ::nonevar ::ActivatorItem_var 1 True ;@line 54
            CALLMETHOD RemoveModFromInventoryItem akTarget ::nonevar CurrentWeapon ::GrenadeLauncher_var ;@line 57
            CAST ::temp16 akTarget ;@line 62
            CALLMETHOD WaitFor3DLoad ::temp16 ::temp17  ;@line 62
            CALLMETHOD PlayIdleAction akTarget ::temp17 ::ActionCustomGrenadeLauncherFrom_var None ;@line 64
            JUMP label1
            label2:
            CAST ::temp18 "No underbarrel attachments found." ;@line 69
            CALLSTATIC code:log WriteLine ::temp17 Log ::temp18 2 ;@line 69
            CALLMETHOD AddItem akTarget ::nonevar ::ActivatorItem_var 1 True ;@line 70
            label1:
            JUMP label0
            label8:
            CALLMETHOD WornHasKeyword akTarget ::temp6 ::AnimsMK01_var ;@line 72
            JUMPF ::temp6 label7 ;@line 72
            CALLMETHOD WornHasKeyword akTarget ::temp7 ::AnimsActive_var ;@line 74
            NOT ::temp19 ::temp7 ;@line 74
            JUMPF ::temp19 label6 ;@line 74
            CAST ::temp20 akTarget ;@line 76
            STRCAT ::temp21 ::temp20 " MasterKey is now active on their " ;@line 76
            CAST ::temp22 CurrentWeapon ;@line 76
            STRCAT ::temp23 ::temp21 ::temp22 ;@line 76
            STRCAT ::temp24 ::temp23 " AKA: " ;@line 76
            STRCAT ::temp25 ::temp24 CurrentWeaponName ;@line 76
            CAST ::temp18 ::temp25 ;@line 76
            CALLSTATIC code:log WriteLine ::temp26 Log ::temp18 0 ;@line 76
            CALLMETHOD AddItem akTarget ::nonevar ::ActivatorItem_var 1 True ;@line 77
            CALLMETHOD AttachModToInventoryItem akTarget ::temp26 CurrentWeapon ::MasterKey_var ;@line 80
            CAST ::temp27 akTarget ;@line 83
            CALLMETHOD WaitFor3DLoad ::temp27 ::temp26  ;@line 83
            CALLMETHOD PlayIdleAction akTarget ::temp26 ::ActionCustomMasterKeyTo_var None ;@line 85
            JUMP label4
            label6:
            CALLMETHOD WornHasKeyword akTarget ::temp26 ::AnimsActive_var ;@line 88
            JUMPF ::temp26 label5 ;@line 88
            CAST ::temp20 akTarget ;@line 90
            STRCAT ::temp21 ::temp20 " MasterKey is now inactive on their " ;@line 90
            CAST ::temp22 CurrentWeapon ;@line 90
            STRCAT ::temp23 ::temp21 ::temp22 ;@line 90
            STRCAT ::temp24 ::temp23 " AKA: " ;@line 90
            STRCAT ::temp25 ::temp24 CurrentWeaponName ;@line 90
            CAST ::temp28 ::temp25 ;@line 90
            CALLSTATIC code:log WriteLine ::temp29 Log ::temp28 0 ;@line 90
            CALLMETHOD AddItem akTarget ::nonevar ::ActivatorItem_var 1 True ;@line 91
            CALLMETHOD RemoveModFromInventoryItem akTarget ::nonevar CurrentWeapon ::MasterKey_var ;@line 94
            CAST ::temp27 akTarget ;@line 97
            CALLMETHOD WaitFor3DLoad ::temp27 ::temp29  ;@line 97
            CALLMETHOD PlayIdleAction akTarget ::temp29 ::ActionCustomMasterKeyFrom_var None ;@line 99
            JUMP label4
            label5:
            CAST ::temp28 "No underbarrel attachments found." ;@line 104
            CALLSTATIC code:log WriteLine ::temp29 Log ::temp28 2 ;@line 104
            CALLMETHOD AddItem akTarget ::nonevar ::ActivatorItem_var 1 True ;@line 105
            label4:
            JUMP label0
            label7:
            CAST ::temp30 "No underbarrel attachments found." ;@line 109
            CALLSTATIC code:log WriteLine ::temp19 Log ::temp30 2 ;@line 109
            CALLMETHOD AddItem akTarget ::nonevar ::ActivatorItem_var 1 True ;@line 110
            label0:
          .endCode
        .endFunction
        .function OnEffectFinish 
          .userFlags 0
          .docString ""
          .return NONE
          .paramTable
            .param akTarget actor
            .param akCaster actor
          .endParamTable
          .localTable
            .local ::nonevar none
          .endLocalTable
          .code
            CALLMETHOD SetValue akTarget ::nonevar ::SpecialEquip_var 0.0 ;@line 116
          .endCode
        .endFunction
      .endState
    .endStateTable
  .endObject
.endObjectTable