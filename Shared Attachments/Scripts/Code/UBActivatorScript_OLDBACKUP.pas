.info
  .source "F:\\Mod Stuff\\Fallout 4\\MO2\\mods\\Code Shared Attachments\\Scripts\\Source\\User\\Code\\UBActivatorScript_OLDBACKUP.psc"
  .modifyTime 1646172964
  .compileTime 1651164048
  .user "Sam"
  .computer "SAMS_DESKTOP"
.endInfo
.userFlagsRef
  .flag default 2
  .flag mandatory 5
  .flag collapsedonbase 4
  .flag hidden 0
  .flag collapsedonref 3
  .flag conditional 1
.endUserFlagsRef
.objectTable
  .object Code:UBActivatorScript_OLDBACKUP activemagiceffect 
    .userFlags 0
    .docString ""
    .autoState 
    .structTable
    .endStructTable
    .variableTable
      .variable ::ActivatorItem_var form 
        .userFlags 0
        .initialValue None
      .endVariable
      .variable ::AnimGL_var keyword const
        .userFlags 0
        .initialValue None
      .endVariable
      .variable ::AnimMK_var keyword const
        .userFlags 0
        .initialValue None
      .endVariable
      .variable ::ActiveUB_var keyword const
        .userFlags 0
        .initialValue None
      .endVariable
      .variable ::MasterKey_var objectmod const
        .userFlags 0
        .initialValue None
      .endVariable
      .variable ::GrenadeLauncher_var objectmod const
        .userFlags 0
        .initialValue None
      .endVariable
      .variable Player actor 
        .userFlags 0
        .initialValue None
      .endVariable
      .variable CurrentWeapon weapon 
        .userFlags 0
        .initialValue None
      .endVariable
    .endVariableTable
    .propertyTable
	  .property ActivatorItem form auto
	    .userFlags 0
	    .docString ""
	    .autoVar ::ActivatorItem_var
	  .endProperty
	  .property AnimGL keyword auto
	    .userFlags 32
	    .docString ""
	    .autoVar ::AnimGL_var
	  .endProperty
	  .property AnimMK keyword auto
	    .userFlags 32
	    .docString ""
	    .autoVar ::AnimMK_var
	  .endProperty
	  .property ActiveUB keyword auto
	    .userFlags 32
	    .docString ""
	    .autoVar ::ActiveUB_var
	  .endProperty
	  .property MasterKey objectmod auto
	    .userFlags 32
	    .docString ""
	    .autoVar ::MasterKey_var
	  .endProperty
	  .property GrenadeLauncher objectmod auto
	    .userFlags 32
	    .docString ""
	    .autoVar ::GrenadeLauncher_var
	  .endProperty
    .endPropertyTable
    .propertyGroupTable
      .propertyGroup Properties
        .userFlags 0
        .docString ""
        .property ActivatorItem
        .property AnimGL
        .property AnimMK
        .property ActiveUB
        .property MasterKey
        .property GrenadeLauncher
      .endPropertyGroup
    .endPropertyGroupTable
    .stateTable
      .state
        .function OnEffectStart 
          .userFlags 0
          .docString ""
          .return NONE
          .paramTable
            .param akTarget actor
            .param akCaster actor
          .endParamTable
          .localTable
            .local ::temp2 bool
            .local ::temp3 bool
            .local ::temp4 bool
            .local ::temp5 bool
            .local ::temp8 bool
            .local ::temp9 bool
            .local ::temp10 bool
            .local ::temp6 form
            .local ::temp7 bool
            .local ::nonevar none
            .local ::temp11 bool
            .local ::temp12 form
          .endLocalTable
          .code
            CALLSTATIC game GetPlayer Player  ;@line 17

            CALLMETHOD GetEquippedWeapon Player CurrentWeapon 0 ;@line 18

            CALLMETHOD WornHasKeyword Player ::temp2 ::AnimGL_var ;@line 19
            CAST ::temp5 ::temp2 ;@line 19
            JUMPF ::temp5 label1 ;@line 19
            CALLMETHOD WornHasKeyword Player ::temp3 ::ActiveUB_var ;@line 19
            NOT ::temp4 ::temp3 ;@line 19
            CAST ::temp5 ::temp4 ;@line 19
            label1:
            JUMPF ::temp5 label4 ;@line 19
            CAST ::temp6 CurrentWeapon ;@line 20
            CALLMETHOD AttachModToInventoryItem Player ::temp7 ::temp6 ::GrenadeLauncher_var ;@line 20
            CALLMETHOD AddKeyword Player ::nonevar ::ActiveUB_var ;@line 21
            CALLMETHOD AddItem Player ::nonevar ::ActivatorItem_var 1 True ;@line 22
            CALLSTATIC debug Notification ::nonevar "Grenade Launcher Active" ;@line 23
            JUMP label0
            label4:
            CALLMETHOD WornHasKeyword player ::temp7 ::AnimMK_var ;@line 24
            CAST ::temp10 ::temp7 ;@line 24
            JUMPF ::temp10 label2 ;@line 24
            CALLMETHOD WornHasKeyword Player ::temp8 ::ActiveUB_var ;@line 24
            NOT ::temp9 ::temp8 ;@line 24
            CAST ::temp10 ::temp9 ;@line 24
            label2:
            JUMPF ::temp10 label3 ;@line 24
            CAST ::temp6 CurrentWeapon ;@line 25
            CALLMETHOD AttachModToInventoryItem Player ::temp11 ::temp6 ::MasterKey_var ;@line 25
            CALLMETHOD AddKeyword Player ::nonevar ::ActiveUB_var ;@line 26
            CALLMETHOD AddItem Player ::nonevar ::ActivatorItem_var 1 True ;@line 27
            CALLSTATIC debug Notification ::nonevar "Grenade Launcher Active" ;@line 28
            JUMP label0
            label3:
            CAST ::temp12 CurrentWeapon ;@line 30
            CALLMETHOD RemoveModFromInventoryItem Player ::nonevar ::temp12 ::MasterKey_var ;@line 30
            CAST ::temp12 CurrentWeapon ;@line 31
            CALLMETHOD RemoveModFromInventoryItem Player ::nonevar ::temp12 ::GrenadeLauncher_var ;@line 31
            CALLMETHOD RemoveKeyword Player ::nonevar ::ActiveUB_var ;@line 32
            CALLMETHOD AddItem Player ::nonevar ::ActivatorItem_var 1 True ;@line 33
            label0:
          .endCode
        .endFunction
      .endState
    .endStateTable
  .endObject
.endObjectTable