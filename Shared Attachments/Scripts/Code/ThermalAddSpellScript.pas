.info
  .source "F:\\Mod Stuff\\Fallout 4\\MO2\\mods\\Code Shared Attachments\\Scripts\\Source\\User\\Code\\ThermalAddSpellScript.psc"
  .modifyTime 1644726595
  .compileTime 1651164048
  .user "Sam"
  .computer "SAMS_DESKTOP"
.endInfo
.userFlagsRef
  .flag default 2
  .flag mandatory 5
  .flag collapsedonbase 4
  .flag hidden 0
  .flag collapsedonref 3
  .flag conditional 1
.endUserFlagsRef
.objectTable
  .object Code:ThermalAddSpellScript ActiveMagicEffect 
    .userFlags 0
    .docString ""
    .autoState 
    .structTable
    .endStructTable
    .variableTable
      .variable ::ThermalProjectileSpell_var spell 
        .userFlags 0
        .initialValue None
      .endVariable
    .endVariableTable
    .propertyTable
	  .property ThermalProjectileSpell spell auto
	    .userFlags 0
	    .docString ""
	    .autoVar ::ThermalProjectileSpell_var
	  .endProperty
    .endPropertyTable
    .propertyGroupTable
      .propertyGroup 
        .userFlags 0
        .docString ""
        .property ThermalProjectileSpell
      .endPropertyGroup
    .endPropertyGroupTable
    .stateTable
      .state
        .function OnEffectStart 
          .userFlags 0
          .docString ""
          .return NONE
          .paramTable
            .param akTarget actor
            .param akCaster actor
          .endParamTable
          .localTable
            .local ::temp0 bool
          .endLocalTable
          .code
            CALLMETHOD AddSpell akTarget ::temp0 ::ThermalProjectileSpell_var true ;@line 7
          .endCode
        .endFunction
        .function OnEffectFinish 
          .userFlags 0
          .docString ""
          .return NONE
          .paramTable
            .param akTarget actor
            .param akCaster actor
          .endParamTable
          .localTable
            .local ::temp1 bool
          .endLocalTable
          .code
            CALLMETHOD RemoveSpell akTarget ::temp1 ::ThermalProjectileSpell_var ;@line 12
          .endCode
        .endFunction
      .endState
    .endStateTable
  .endObject
.endObjectTable