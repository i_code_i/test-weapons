.info
  .source "F:\\Mod Stuff\\Fallout 4\\MO2\\mods\\Code Shared Attachments\\Scripts\\Source\\User\\Code\\AttachCosmeticArmor.psc"
  .modifyTime 1651163990
  .compileTime 1651164048
  .user "Sam"
  .computer "SAMS_DESKTOP"
.endInfo
.userFlagsRef
  .flag default 2
  .flag mandatory 5
  .flag collapsedonbase 4
  .flag hidden 0
  .flag collapsedonref 3
  .flag conditional 1
.endUserFlagsRef
.objectTable
  .object Code:AttachCosmeticArmor ActiveMagicEffect 
    .userFlags 0
    .docString "Attach Armor to actor"
    .autoState 
    .structTable
    .endStructTable
    .variableTable
      .variable ::CharmArmor_var armor const
        .userFlags 0
        .initialValue None
      .endVariable
    .endVariableTable
    .propertyTable
	  .property CharmArmor armor auto
	    .userFlags 0
	    .docString "armor to equip"
	    .autoVar ::CharmArmor_var
	  .endProperty
    .endPropertyTable
    .propertyGroupTable
      .propertyGroup 
        .userFlags 0
        .docString ""
        .property CharmArmor
      .endPropertyGroup
    .endPropertyGroupTable
    .stateTable
      .state
        .function OnEffectStart 
          .userFlags 0
          .docString ""
          .return NONE
          .paramTable
            .param akTarget actor
            .param akCaster actor
          .endParamTable
          .localTable
            .local ::temp0 form
            .local ::nonevar none
          .endLocalTable
          .code
            CAST ::temp0 ::CharmArmor_var ;@line 9
            CALLMETHOD EquipItem akCaster ::nonevar ::temp0 False True ;@line 9
          .endCode
        .endFunction
        .function OnEffectFinish 
          .userFlags 0
          .docString ""
          .return NONE
          .paramTable
            .param akTarget actor
            .param akCaster actor
          .endParamTable
          .localTable
            .local ::temp1 form
            .local ::nonevar none
          .endLocalTable
          .code
            CAST ::temp1 ::CharmArmor_var ;@line 13
            CALLMETHOD RemoveItem akCaster ::nonevar ::temp1 1 True None ;@line 13
          .endCode
        .endFunction
      .endState
    .endStateTable
  .endObject
.endObjectTable