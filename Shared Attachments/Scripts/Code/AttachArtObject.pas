.info
  .source "F:\\Mod Stuff\\Fallout 4\\MO2\\mods\\Code Shared Attachments\\Scripts\\Source\\User\\Code\\AttachArtObject.psc"
  .modifyTime 1651093141
  .compileTime 1651164003
  .user "Sam"
  .computer "SAMS_DESKTOP"
.endInfo
.userFlagsRef
  .flag default 2
  .flag mandatory 5
  .flag collapsedonbase 4
  .flag hidden 0
  .flag collapsedonref 3
  .flag conditional 1
.endUserFlagsRef
.objectTable
  .object Code:AttachArtObject ActiveMagicEffect 
    .userFlags 0
    .docString "Attach VisualEffect to a named node"
    .autoState 
    .structTable
    .endStructTable
    .variableTable
      .variable ::AttachVE_var visualeffect const
        .userFlags 0
        .initialValue None
      .endVariable
      .variable ::myPlaceAtNode_var string const
        .userFlags 0
        .initialValue None
      .endVariable
      .variable ::CharmArmor_var armor const
        .userFlags 0
        .initialValue None
      .endVariable
    .endVariableTable
    .propertyTable
	  .property AttachVE visualeffect auto
	    .userFlags 0
	    .docString "attached art"
	    .autoVar ::AttachVE_var
	  .endProperty
	  .property myPlaceAtNode String auto
	    .userFlags 0
	    .docString "node to place the art at"
	    .autoVar ::myPlaceAtNode_var
	  .endProperty
	  .property CharmArmor armor auto
	    .userFlags 0
	    .docString "armor to equip"
	    .autoVar ::CharmArmor_var
	  .endProperty
    .endPropertyTable
    .propertyGroupTable
      .propertyGroup 
        .userFlags 0
        .docString ""
        .property AttachVE
        .property myPlaceAtNode
        .property CharmArmor
      .endPropertyGroup
    .endPropertyGroupTable
    .stateTable
      .state
        .function OnEffectStart 
          .userFlags 0
          .docString ""
          .return NONE
          .paramTable
            .param akTarget actor
            .param akCaster actor
          .endParamTable
          .localTable
            .local ::temp0 actor
            .local ::temp1 form
            .local ::nonevar none
          .endLocalTable
          .code
            CALLSTATIC game GetPlayer ::temp0  ;@line 15
            CAST ::temp1 ::CharmArmor_var ;@line 15
            CALLMETHOD EquipItem ::temp0 ::nonevar ::temp1 False True ;@line 15
          .endCode
        .endFunction
        .function OnEffectFinish 
          .userFlags 0
          .docString ""
          .return NONE
          .paramTable
            .param akTarget actor
            .param akCaster actor
          .endParamTable
          .localTable
            .local ::temp2 objectreference
            .local ::nonevar none
            .local ::temp3 form
          .endLocalTable
          .code
            CAST ::temp2 akCaster ;@line 19
            CALLMETHOD Stop ::AttachVE_var ::nonevar ::temp2 ;@line 19
            CAST ::temp3 ::CharmArmor_var ;@line 20
            CALLMETHOD RemoveItem akCaster ::nonevar ::temp3 1 True None ;@line 20
          .endCode
        .endFunction
      .endState
    .endStateTable
  .endObject
.endObjectTable