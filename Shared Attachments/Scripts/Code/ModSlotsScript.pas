.info
  .source "F:\\Mod Stuff\\Fallout 4\\MO2\\mods\\Code Shared Attachments\\Scripts\\Source\\User\\Code\\ModSlotsScript.psc"
  .modifyTime 1646601110
  .compileTime 1651164048
  .user "Sam"
  .computer "SAMS_DESKTOP"
.endInfo
.userFlagsRef
  .flag default 2
  .flag mandatory 5
  .flag collapsedonbase 4
  .flag hidden 0
  .flag collapsedonref 3
  .flag conditional 1
.endUserFlagsRef
.objectTable
  .object Code:ModSlotsScript Quest 
    .userFlags 0
    .docString ""
    .autoState 
    .structTable
    .endStructTable
    .variableTable
      .variable ::do_ModMenuSlotKeywordList_var formlist 
        .userFlags 0
        .initialValue None
      .endVariable
      .variable ::Code_ModMenuSlotKeywordList_var formlist 
        .userFlags 0
        .initialValue None
      .endVariable
      .variable ::Code_Quest_ModSlots_var quest 
        .userFlags 0
        .initialValue None
      .endVariable
    .endVariableTable
    .propertyTable
	  .property do_ModMenuSlotKeywordList formlist auto
	    .userFlags 0
	    .docString ""
	    .autoVar ::do_ModMenuSlotKeywordList_var
	  .endProperty
	  .property Code_ModMenuSlotKeywordList formlist auto
	    .userFlags 0
	    .docString ""
	    .autoVar ::Code_ModMenuSlotKeywordList_var
	  .endProperty
	  .property Code_Quest_ModSlots quest auto
	    .userFlags 0
	    .docString ""
	    .autoVar ::Code_Quest_ModSlots_var
	  .endProperty
    .endPropertyTable
    .propertyGroupTable
      .propertyGroup Properties
        .userFlags 0
        .docString ""
        .property do_ModMenuSlotKeywordList
        .property Code_ModMenuSlotKeywordList
        .property Code_Quest_ModSlots
      .endPropertyGroup
    .endPropertyGroupTable
    .stateTable
      .state
        .function OnQuestInit 
          .userFlags 0
          .docString ""
          .return NONE
          .paramTable
          .endParamTable
          .localTable
            .local ::nonevar none
          .endLocalTable
          .code
            CALLMETHOD AddKeyordsToFormlist self ::nonevar ::Code_ModMenuSlotKeywordList_var ::do_ModMenuSlotKeywordList_var ;@line 13
            CALLMETHOD Stop ::Code_Quest_ModSlots_var ::nonevar  ;@line 14
          .endCode
        .endFunction
        .function AddKeyordsToFormlist 
          .userFlags 0
          .docString ""
          .return NONE
          .paramTable
            .param KeywordToAddList formlist
            .param KeywordList formlist
          .endParamTable
          .localTable
            .local ::temp0 int
            .local ::temp1 bool
            .local i int
            .local ::temp2 form
            .local ::nonevar none
            .local KeywordToAdd form
          .endLocalTable
          .code
            ASSIGN i 0 ;@line 18
            label0:
            CALLMETHOD GetSize KeywordToAddList ::temp0  ;@line 19
            COMPARELT ::temp1 i ::temp0 ;@line 19
            JUMPF ::temp1 label1 ;@line 19
            CALLMETHOD GetAt KeywordToAddList ::temp2 i ;@line 20
            CAST KeywordToAdd ::temp2 ;@line 20
            CALLMETHOD AddForm KeywordList ::nonevar KeywordToAdd ;@line 21
            IADD i i 1 ;@line 23

            JUMP label0
            label1:
          .endCode
        .endFunction
      .endState
    .endStateTable
  .endObject
.endObjectTable