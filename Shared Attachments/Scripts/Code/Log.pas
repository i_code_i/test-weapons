.info
  .source "F:\\Mod Stuff\\Fallout 4\\MO2\\mods\\Code Shared Attachments\\Scripts\\Source\\User\\Code\\Log.psc"
  .modifyTime 1646607121
  .compileTime 1651164048
  .user "Sam"
  .computer "SAMS_DESKTOP"
.endInfo
.userFlagsRef
  .flag default 2
  .flag mandatory 5
  .flag collapsedonbase 4
  .flag hidden 0
  .flag collapsedonref 3
  .flag conditional 1
.endUserFlagsRef
.objectTable
  .object Code:Log ScriptObject const
    .userFlags 1
    .docString ""
    .autoState 
    .structTable
      .struct UserLog
        .variable Caller string 
          .userFlags 0
          .initialValue None
          .docString ""
        .endVariable
        .variable FileName string 
          .userFlags 0
          .initialValue None
          .docString ""
        .endVariable
      .endStruct
      .struct Point
        .variable X float 
          .userFlags 0
          .initialValue 0.0
          .docString ""
        .endVariable
        .variable Y float 
          .userFlags 0
          .initialValue 0.0
          .docString ""
        .endVariable
        .variable Z float 
          .userFlags 0
          .initialValue 0.0
          .docString ""
        .endVariable
      .endStruct
    .endStructTable
    .variableTable
    .endVariableTable
    .propertyTable
    .endPropertyTable
    .propertyGroupTable
    .endPropertyGroupTable
    .stateTable
      .state
        .function GetLog static
          .userFlags 0
          .docString ""
          .return code:log#userlog
          .paramTable
            .param aScriptObject scriptobject
          .endParamTable
          .localTable
            .local ::temp1 string
            .local contextLog code:log#userlog
          .endLocalTable
          .code
            STRUCTCREATE contextLog ;@line 19
            CAST ::temp1 aScriptObject ;@line 20
            STRUCTSET contextLog Caller ::temp1 ;@line 20
            CALLSTATIC code:log GetTitle ::temp1  ;@line 21
            STRUCTSET contextLog FileName ::temp1 ;@line 21
            RETURN contextLog ;@line 22
          .endCode
        .endFunction
        .function WriteChangedValue static
          .userFlags 0
          .docString ""
          .return NONE
          .paramTable
            .param UserLog code:log#userlog
            .param propertyName string
            .param fromValue var
            .param toValue var
          .endParamTable
          .localTable
            .local ::temp3 string
            .local ::temp4 string
            .local ::temp5 string
            .local ::temp6 string
            .local ::temp7 string
            .local ::temp8 string
            .local ::temp9 string
            .local ::temp10 var
            .local ::temp11 bool
          .endLocalTable
          .code
            STRCAT ::temp3 "Changing " propertyName ;@line 26
            STRCAT ::temp4 ::temp3 " from " ;@line 26
            CAST ::temp5 fromValue ;@line 26
            STRCAT ::temp6 ::temp4 ::temp5 ;@line 26
            STRCAT ::temp7 ::temp6 " to " ;@line 26
            CAST ::temp8 toValue ;@line 26
            STRCAT ::temp9 ::temp7 ::temp8 ;@line 26
            CAST ::temp10 ::temp9 ;@line 26
            CALLSTATIC code:log WriteLine ::temp11 UserLog ::temp10 0 ;@line 26
          .endCode
        .endFunction
        .function WriteLine static
          .userFlags 0
          .docString ""
          .return bool
          .paramTable
            .param UserLog code:log#userlog
            .param text var
            .param aiSeverity int
          .endParamTable
          .localTable
            .local ::temp13 bool
            .local ::temp16 bool
            .local ::temp18 string
            .local ::temp19 string
            .local ::temp20 string
            .local defaultFile string
            .local ::temp15 string
            .local ::temp17 string
          .endLocalTable
          .code
            ASSIGN defaultFile "Code_Log" ;@line 30
            COMPAREEQ ::temp13 UserLog None ;@line 31
            JUMPF ::temp13 label2 ;@line 31
            STRUCTCREATE UserLog ;@line 32

            ASSIGN ::temp15 "" ;@line 33
            STRUCTSET UserLog Caller ::temp15 ;@line 33
            ASSIGN ::temp15 defaultFile ;@line 34
            STRUCTSET UserLog FileName ::temp15 ;@line 34
            JUMP label0
            label2:
            STRUCTGET ::temp15 UserLog FileName ;@line 35
            CALLSTATIC code:log StringIsNoneOrEmpty ::temp16 ::temp15 ;@line 35
            JUMPF ::temp16 label1 ;@line 35
            ASSIGN ::temp17 defaultFile ;@line 36
            STRUCTSET UserLog FileName ::temp17 ;@line 36
            JUMP label0
            label1:
            label0:
            STRUCTGET ::temp17 UserLog Caller ;@line 38
            STRCAT ::temp18 ::temp17 " " ;@line 38
            CAST ::temp19 text ;@line 38
            STRCAT ::temp20 ::temp18 ::temp19 ;@line 38
            CAST text ::temp20 ;@line 38

            STRUCTGET ::temp18 UserLog FileName ;@line 39
            CAST ::temp19 text ;@line 39
            CALLSTATIC code:log Write ::temp13 ::temp18 ::temp19 aiSeverity ;@line 39
            RETURN ::temp13 ;@line 39
          .endCode
        .endFunction
        .function WriteMessage static
          .userFlags 0
          .docString ""
          .return NONE
          .paramTable
            .param UserLog code:log#userlog
            .param text var
          .endParamTable
          .localTable
            .local ::temp22 bool
            .local ::temp23 string
            .local ::nonevar none
          .endLocalTable
          .code
            CALLSTATIC code:log WriteLine ::temp22 UserLog text 0 ;@line 43
            JUMPF ::temp22 label4 ;@line 43
            CAST ::temp23 text ;@line 44
            CALLSTATIC debug MessageBox ::nonevar ::temp23 ;@line 44
            JUMP label3
            label4:
            label3:
          .endCode
        .endFunction
        .function WriteNotification static
          .userFlags 0
          .docString ""
          .return NONE
          .paramTable
            .param UserLog code:log#userlog
            .param text var
          .endParamTable
          .localTable
            .local ::temp24 bool
            .local ::temp25 string
            .local ::nonevar none
          .endLocalTable
          .code
            CALLSTATIC code:log WriteLine ::temp24 UserLog text 0 ;@line 49
            JUMPF ::temp24 label6 ;@line 49
            CAST ::temp25 text ;@line 50
            CALLSTATIC debug Notification ::nonevar ::temp25 ;@line 50
            JUMP label5
            label6:
            label5:
          .endCode
        .endFunction
        .function Write static
          .userFlags 0
          .docString ""
          .return bool
          .paramTable
            .param FileName string
            .param text string
            .param aiSeverity int
          .endParamTable
          .localTable
            .local ::temp26 bool
            .local ::temp27 bool
          .endLocalTable
          .code
            CALLSTATIC debug TraceUser ::temp26 FileName text aiSeverity ;@line 55
            JUMPF ::temp26 label8 ;@line 55
            RETURN True ;@line 56
            JUMP label7
            label8:
            CALLSTATIC debug OpenUserLog ::temp27 FileName ;@line 58
            CALLSTATIC debug TraceUser ::temp27 FileName text aiSeverity ;@line 59
            RETURN ::temp27 ;@line 59
            label7:
          .endCode
        .endFunction
        .function GetTitle static
          .userFlags 0
          .docString ""
          .return string
          .paramTable
          .endParamTable
          .localTable
          .endLocalTable
          .code
            RETURN "Code Shared Attachments" ;@line 66
          .endCode
        .endFunction
        .function ChangeState static
          .userFlags 0
          .docString ""
          .return bool
          .paramTable
            .param aScriptObject scriptobject
            .param asNewState string
          .endParamTable
          .localTable
            .local ::temp29 bool
            .local stateName string
            .local ::nonevar none
          .endLocalTable
          .code
            CALLMETHOD GetState aScriptObject stateName  ;@line 70
            COMPAREEQ ::temp29 stateName asNewState ;@line 71
            NOT ::temp29 ::temp29 ;@line 71
            JUMPF ::temp29 label10 ;@line 71
            CALLMETHOD GoToState aScriptObject ::nonevar asNewState ;@line 72
            RETURN True ;@line 73
            JUMP label9
            label10:
            RETURN False ;@line 75
            label9:
          .endCode
        .endFunction
        .function StringIsNoneOrEmpty static
          .userFlags 0
          .docString ""
          .return bool
          .paramTable
            .param value string
          .endParamTable
          .localTable
            .local ::temp30 bool
            .local ::temp31 bool
            .local ::temp32 bool
          .endLocalTable
          .code
            NOT ::temp30 value ;@line 80
            CAST ::temp32 ::temp30 ;@line 80
            JUMPT ::temp32 label11 ;@line 80
            COMPAREEQ ::temp31 value "" ;@line 80
            CAST ::temp32 ::temp31 ;@line 80
            label11:
            RETURN ::temp32 ;@line 80
          .endCode
        .endFunction
        .function HasState static
          .userFlags 0
          .docString ""
          .return bool
          .paramTable
            .param aScriptObject scriptobject
          .endParamTable
          .localTable
            .local ::temp33 string
            .local ::temp34 bool
          .endLocalTable
          .code
            CALLMETHOD GetState aScriptObject ::temp33  ;@line 84
            COMPAREEQ ::temp34 ::temp33 "" ;@line 84
            NOT ::temp34 ::temp34 ;@line 84
            RETURN ::temp34 ;@line 84
          .endCode
        .endFunction
        .function BoolToInt static
          .userFlags 0
          .docString ""
          .return int
          .paramTable
            .param value bool
          .endParamTable
          .localTable
          .endLocalTable
          .code
            JUMPF value label13 ;@line 88
            RETURN 1 ;@line 89
            JUMP label12
            label13:
            RETURN 0 ;@line 91
            label12:
          .endCode
        .endFunction
        .function PointToString static
          .userFlags 0
          .docString ""
          .return string
          .paramTable
            .param value code:log#point
          .endParamTable
          .localTable
            .local ::temp35 float
            .local ::temp36 string
            .local ::temp37 string
            .local ::temp38 string
            .local ::temp39 float
            .local ::temp40 string
            .local ::temp41 string
            .local ::temp42 string
            .local ::temp43 float
            .local ::temp44 string
            .local ::temp45 string
          .endLocalTable
          .code
            STRUCTGET ::temp35 value X ;@line 96
            CAST ::temp36 ::temp35 ;@line 96
            STRCAT ::temp37 "X:" ::temp36 ;@line 96
            STRCAT ::temp38 ::temp37 ", Y:" ;@line 96
            STRUCTGET ::temp39 value Y ;@line 96
            CAST ::temp40 ::temp39 ;@line 96
            STRCAT ::temp41 ::temp38 ::temp40 ;@line 96
            STRCAT ::temp42 ::temp41 ", Z:" ;@line 96
            STRUCTGET ::temp43 value Z ;@line 96
            CAST ::temp44 ::temp43 ;@line 96
            STRCAT ::temp45 ::temp42 ::temp44 ;@line 96
            RETURN ::temp45 ;@line 96
          .endCode
        .endFunction
      .endState
    .endStateTable
  .endObject
.endObjectTable