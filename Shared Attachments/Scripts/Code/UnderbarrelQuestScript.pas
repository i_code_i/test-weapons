.info
  .source "F:\\Mod Stuff\\Fallout 4\\MO2\\mods\\Code Shared Attachments\\Scripts\\Source\\User\\Code\\UnderbarrelQuestScript.psc"
  .modifyTime 1646966420
  .compileTime 1651164049
  .user "Sam"
  .computer "SAMS_DESKTOP"
.endInfo
.userFlagsRef
  .flag default 2
  .flag mandatory 5
  .flag collapsedonbase 4
  .flag hidden 0
  .flag collapsedonref 3
  .flag conditional 1
.endUserFlagsRef
.objectTable
  .object Code:UnderBarrelQuestScript ReferenceAlias 
    .userFlags 0
    .docString ""
    .autoState 
    .structTable
    .endStructTable
    .variableTable
      .variable ::IsGrenadeLauncher_var globalvariable 
        .userFlags 0
        .initialValue None
      .endVariable
      .variable ::IsMasterKey_var globalvariable 
        .userFlags 0
        .initialValue None
      .endVariable
      .variable ::IsAttaching_var globalvariable 
        .userFlags 0
        .initialValue None
      .endVariable
      .variable ::AnimsActive_var keyword 
        .userFlags 0
        .initialValue None
      .endVariable
      .variable ::AnimsGL_var keyword 
        .userFlags 0
        .initialValue None
      .endVariable
      .variable ::AnimsMK01_var keyword 
        .userFlags 0
        .initialValue None
      .endVariable
      .variable Log code:log#userlog 
        .userFlags 0
        .initialValue None
      .endVariable
    .endVariableTable
    .propertyTable
	  .property IsGrenadeLauncher globalvariable auto
	    .userFlags 0
	    .docString "Value for if the player has a grenade launcher. 0 = No, 1 = Yes"
	    .autoVar ::IsGrenadeLauncher_var
	  .endProperty
	  .property IsMasterKey globalvariable auto
	    .userFlags 0
	    .docString "Value for if the player has a masterkey. 0 = No, 1 = Yes"
	    .autoVar ::IsMasterKey_var
	  .endProperty
	  .property IsAttaching globalvariable auto
	    .userFlags 0
	    .docString "Value for if the script should attach the omod. 0 = Remove, 1 = Add"
	    .autoVar ::IsAttaching_var
	  .endProperty
	  .property AnimsActive keyword auto
	    .userFlags 0
	    .docString ""
	    .autoVar ::AnimsActive_var
	  .endProperty
	  .property AnimsGL keyword auto
	    .userFlags 0
	    .docString ""
	    .autoVar ::AnimsGL_var
	  .endProperty
	  .property AnimsMK01 keyword auto
	    .userFlags 0
	    .docString ""
	    .autoVar ::AnimsMK01_var
	  .endProperty
    .endPropertyTable
    .propertyGroupTable
      .propertyGroup Properties
        .userFlags 0
        .docString ""
        .property IsGrenadeLauncher
        .property IsMasterKey
        .property IsAttaching
        .property AnimsActive
        .property AnimsGL
        .property AnimsMK01
      .endPropertyGroup
    .endPropertyGroupTable
    .stateTable
      .state
        .function OnAliasInit 
          .userFlags 0
          .docString ""
          .return NONE
          .paramTable
          .endParamTable
          .localTable
            .local ::temp1 string
          .endLocalTable
          .code
            STRUCTCREATE Log ;@line 21

            CAST ::temp1 self ;@line 22
            STRUCTSET Log Caller ::temp1 ;@line 22
            ASSIGN ::temp1 "Code_UnderBarrel" ;@line 23
            STRUCTSET Log FileName ::temp1 ;@line 23
          .endCode
        .endFunction
        .function OnItemEquipped 
          .userFlags 0
          .docString ""
          .return NONE
          .paramTable
            .param akBaseObject form
            .param akReference objectreference
          .endParamTable
          .localTable
            .local ::temp3 scriptobject
            .local ::temp5 string
            .local ::temp8 weapon
            .local Player actor
            .local CurrentWeapon form
            .local CurrentWeaponName String
            .local ::temp9 weapon
            .local ::temp11 string
            .local ::temp12 string
            .local ::temp13 string
            .local ::temp14 string
            .local ::temp15 var
            .local ::temp16 bool
            .local ::nonevar none
            .local ::temp17 bool
            .local ::temp18 bool
            .local ::temp19 bool
            .local ::temp20 bool
          .endLocalTable
          .code
            CAST ::temp3 self ;@line 27
            CALLSTATIC code:log GetLog Log ::temp3 ;@line 27

            ASSIGN ::temp5 "Code_UnderBarrel" ;@line 28
            STRUCTSET Log FileName ::temp5 ;@line 28
            CALLSTATIC game GetPlayer Player  ;@line 30
            ASSIGN CurrentWeapon none ;@line 31
            ASSIGN CurrentWeaponName "" ;@line 32
            CALLMETHOD GetEquippedWeapon Player ::temp8 0 ;@line 34
            JUMPF ::temp8 label1 ;@line 34
            CALLMETHOD GetEquippedWeapon Player ::temp9 0 ;@line 35
            CAST CurrentWeapon ::temp9 ;@line 35

            CALLMETHOD GetName CurrentWeapon CurrentWeaponName  ;@line 36

            CAST ::temp11 CurrentWeapon ;@line 37
            STRCAT ::temp12 "Equiping " ::temp11 ;@line 37
            STRCAT ::temp13 ::temp12 " AKA: " ;@line 37
            STRCAT ::temp14 ::temp13 CurrentWeaponName ;@line 37
            CAST ::temp15 ::temp14 ;@line 37
            CALLSTATIC code:log WriteLine ::temp16 Log ::temp15 0 ;@line 37
            JUMP label0
            label1:
            label0:
            CALLMETHOD WornHasKeyword Player ::temp16 ::AnimsGL_var ;@line 40
            JUMPF ::temp16 label10 ;@line 40
            CALLMETHOD SetValue ::IsGrenadeLauncher_var ::nonevar 1.0 ;@line 42
            CALLMETHOD SetValue ::IsMasterKey_var ::nonevar 0.0 ;@line 43
            CALLMETHOD WornHasKeyword Player ::temp17 ::AnimsActive_var ;@line 44
            JUMPF ::temp17 label5 ;@line 44
            CALLMETHOD SetValue ::IsAttaching_var ::nonevar 0.0 ;@line 46
            JUMP label3
            label5:
            CALLMETHOD WornHasKeyword Player ::temp18 ::AnimsActive_var ;@line 47
            NOT ::temp19 ::temp18 ;@line 47
            JUMPF ::temp19 label4 ;@line 47
            CALLMETHOD SetValue ::IsAttaching_var ::nonevar 1.0 ;@line 49
            JUMP label3
            label4:
            label3:
            JUMP label2
            label10:
            CALLMETHOD WornHasKeyword Player ::temp17 ::AnimsMK01_var ;@line 53
            JUMPF ::temp17 label9 ;@line 53
            CALLMETHOD SetValue ::IsMasterKey_var ::nonevar 1.0 ;@line 55
            CALLMETHOD SetValue ::IsGrenadeLauncher_var ::nonevar 0.0 ;@line 56
            CALLMETHOD WornHasKeyword Player ::temp18 ::AnimsActive_var ;@line 57
            JUMPF ::temp18 label8 ;@line 57
            CALLMETHOD SetValue ::IsAttaching_var ::nonevar 0.0 ;@line 59
            JUMP label6
            label8:
            CALLMETHOD WornHasKeyword Player ::temp19 ::AnimsActive_var ;@line 60
            NOT ::temp20 ::temp19 ;@line 60
            JUMPF ::temp20 label7 ;@line 60
            CALLMETHOD SetValue ::IsAttaching_var ::nonevar 1.0 ;@line 62
            JUMP label6
            label7:
            label6:
            JUMP label2
            label9:
            label2:
          .endCode
        .endFunction
      .endState
    .endStateTable
  .endObject
.endObjectTable