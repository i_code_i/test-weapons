.info
  .source "F:\\Mod Stuff\\Fallout 4\\MO2\\mods\\Code Shared Attachments\\Scripts\\Source\\User\\Code\\DebugQuestScript.psc"
  .modifyTime 1646793522
  .compileTime 1651164048
  .user "Sam"
  .computer "SAMS_DESKTOP"
.endInfo
.userFlagsRef
  .flag default 2
  .flag mandatory 5
  .flag collapsedonbase 4
  .flag hidden 0
  .flag collapsedonref 3
  .flag conditional 1
.endUserFlagsRef
.objectTable
  .object Code:DebugQuestScript ReferenceAlias 
    .userFlags 0
    .docString ""
    .autoState 
    .structTable
    .endStructTable
    .variableTable
      .variable Log code:log#userlog 
        .userFlags 0
        .initialValue None
      .endVariable
    .endVariableTable
    .propertyTable
	  .property Player actor
	    .userFlags 1
	    .docString ""
	    .function Get 
	      .userFlags 0
	      .docString ""
	      .return actor
	      .paramTable
	      .endParamTable
	      .localTable
	        .local ::temp0 actor
	      .endLocalTable
	      .code
	        CALLSTATIC game GetPlayer ::temp0  ;@line 11
	        RETURN ::temp0 ;@line 11
	      .endCode
	    .endFunction
	  .endProperty
	  .property StateID int
	    .userFlags 1
	    .docString ""
	    .function Get 
	      .userFlags 0
	      .docString ""
	      .return int
	      .paramTable
	      .endParamTable
	      .localTable
	        .local ::temp1 actor
	        .local ::temp2 int
	      .endLocalTable
	      .code
	        PROPGET Player self ::temp1 ;@line 16
	        CALLMETHOD GetAnimationVariableInt ::temp1 ::temp2 "iSyncWeaponDrawState" ;@line 16
	        RETURN ::temp2 ;@line 16
	      .endCode
	    .endFunction
	  .endProperty
	  .property StateIDName string
	    .userFlags 1
	    .docString ""
	    .function Get 
	      .userFlags 0
	      .docString ""
	      .return string
	      .paramTable
	      .endParamTable
	      .localTable
	        .local ::temp3 int
	        .local ::temp4 string
	      .endLocalTable
	      .code
	        PROPGET StateID self ::temp3 ;@line 21
	        CALLMETHOD GetWeaponDrawState self ::temp4 ::temp3 ;@line 21
	        RETURN ::temp4 ;@line 21
	      .endCode
	    .endFunction
	  .endProperty
    .endPropertyTable
    .propertyGroupTable
      .propertyGroup Properties
        .userFlags 0
        .docString ""
        .property Player
        .property StateID
        .property StateIDName
      .endPropertyGroup
    .endPropertyGroupTable
    .stateTable
      .state
        .function OnAliasInit 
          .userFlags 0
          .docString ""
          .return NONE
          .paramTable
          .endParamTable
          .localTable
            .local ::temp6 string
            .local ::temp8 bool
            .local ::temp9 var
          .endLocalTable
          .code
            STRUCTCREATE Log ;@line 27

            CAST ::temp6 self ;@line 28
            STRUCTSET Log Caller ::temp6 ;@line 28
            ASSIGN ::temp6 "Code_UnderBarrel" ;@line 29
            STRUCTSET Log FileName ::temp6 ;@line 29
            CALLMETHOD Register self ::temp8  ;@line 30
            CAST ::temp9 "This alias is initialized" ;@line 31
            CALLSTATIC code:log WriteLine ::temp8 Log ::temp9 0 ;@line 31
          .endCode
        .endFunction
        .function OnAnimationEvent 
          .userFlags 0
          .docString ""
          .return NONE
          .paramTable
            .param akSource objectreference
            .param asEventName string
          .endParamTable
          .localTable
            .local ::temp10 string
            .local ::temp11 string
            .local ::temp12 string
            .local ::temp13 var
            .local ::temp14 bool
          .endLocalTable
          .code
            CAST ::temp10 akSource ;@line 35
            STRCAT ::temp11 ::temp10 " received anim event: " ;@line 35
            STRCAT ::temp12 ::temp11 asEventName ;@line 35
            CAST ::temp13 ::temp12 ;@line 35
            CALLSTATIC code:log WriteLine ::temp14 Log ::temp13 0 ;@line 35
          .endCode
        .endFunction
        .function OnPlayerLoadGame 
          .userFlags 0
          .docString ""
          .return NONE
          .paramTable
          .endParamTable
          .localTable
            .local ::temp15 scriptobject
            .local ::temp17 string
            .local ::temp18 bool
          .endLocalTable
          .code
            CAST ::temp15 self ;@line 40
            CALLSTATIC code:log GetLog Log ::temp15 ;@line 40

            ASSIGN ::temp17 "Code_UnderBarrel" ;@line 41
            STRUCTSET Log FileName ::temp17 ;@line 41
            CALLMETHOD Register self ::temp18  ;@line 42
          .endCode
        .endFunction
        .function GetWeaponDrawState 
          .userFlags 0
          .docString ""
          .return String
          .paramTable
            .param i int
          .endParamTable
          .localTable
            .local ::temp19 bool
            .local ::temp20 bool
            .local ::temp21 bool
            .local ::temp22 bool
            .local ::temp23 bool
            .local ::temp24 bool
            .local ::temp25 bool
            .local ::temp26 bool
            .local ::temp27 bool
            .local ::temp28 bool
            .local ::temp29 bool
            .local ::temp30 bool
            .local ::temp31 bool
            .local ::temp32 var
            .local ::temp33 bool
          .endLocalTable
          .code
            COMPAREEQ ::temp19 i 0 ;@line 58
            JUMPF ::temp19 label1 ;@line 58
            RETURN "WPNEquipFast" ;@line 59
            JUMP label0
            label1:
            label0:
            COMPAREEQ ::temp19 i 1 ;@line 61
            JUMPF ::temp19 label3 ;@line 61
            RETURN "WPNEquip" ;@line 62
            JUMP label2
            label3:
            label2:
            COMPAREEQ ::temp19 i 2 ;@line 64
            JUMPF ::temp19 label5 ;@line 64
            RETURN "CombatState" ;@line 65
            JUMP label4
            label5:
            label4:
            COMPAREEQ ::temp19 i 4 ;@line 67
            JUMPF ::temp19 label7 ;@line 67
            RETURN "DynamicActivation" ;@line 68
            JUMP label6
            label7:
            label6:
            COMPAREEQ ::temp19 i 16 ;@line 70
            JUMPF ::temp19 label9 ;@line 70
            RETURN "WPNUnEquip" ;@line 71
            JUMP label8
            label9:
            label8:
            COMPAREEQ ::temp19 i 17 ;@line 73
            JUMPF ::temp19 label11 ;@line 73
            RETURN "DynamicActivationAllowMovement" ;@line 74
            JUMP label10
            label11:
            label10:
            COMPAREEQ ::temp19 i 18 ;@line 76
            JUMPF ::temp19 label13 ;@line 76
            RETURN "WPNInstantDown" ;@line 77
            JUMP label12
            label13:
            label12:
            COMPAREEQ ::temp19 i 0 ;@line 79
            NOT ::temp19 ::temp19 ;@line 79
            CAST ::temp21 ::temp19 ;@line 79
            JUMPF ::temp21 label15 ;@line 79
            COMPAREEQ ::temp20 i 1 ;@line 79
            NOT ::temp20 ::temp20 ;@line 79
            CAST ::temp21 ::temp20 ;@line 79
            label15:
            CAST ::temp23 ::temp21 ;@line 79
            JUMPF ::temp23 label16 ;@line 79
            COMPAREEQ ::temp22 i 2 ;@line 79
            NOT ::temp22 ::temp22 ;@line 79
            CAST ::temp23 ::temp22 ;@line 79
            label16:
            CAST ::temp25 ::temp23 ;@line 79
            JUMPF ::temp25 label17 ;@line 79
            COMPAREEQ ::temp24 i 4 ;@line 79
            NOT ::temp24 ::temp24 ;@line 79
            CAST ::temp25 ::temp24 ;@line 79
            label17:
            CAST ::temp27 ::temp25 ;@line 79
            JUMPF ::temp27 label18 ;@line 79
            COMPAREEQ ::temp26 i 16 ;@line 79
            NOT ::temp26 ::temp26 ;@line 79
            CAST ::temp27 ::temp26 ;@line 79
            label18:
            CAST ::temp29 ::temp27 ;@line 79
            JUMPF ::temp29 label19 ;@line 79
            COMPAREEQ ::temp28 i 17 ;@line 79
            NOT ::temp28 ::temp28 ;@line 79
            CAST ::temp29 ::temp28 ;@line 79
            label19:
            CAST ::temp31 ::temp29 ;@line 79
            JUMPF ::temp31 label20 ;@line 79
            COMPAREEQ ::temp30 i 18 ;@line 79
            NOT ::temp30 ::temp30 ;@line 79
            CAST ::temp31 ::temp30 ;@line 79
            label20:
            JUMPF ::temp31 label21 ;@line 79
            CAST ::temp32 "Could not determine the value of iSyncWeaponDrawState" ;@line 80
            CALLSTATIC code:log WriteLine ::temp33 Log ::temp32 2 ;@line 80
            RETURN "Unknown" ;@line 81
            JUMP label14
            label21:
            label14:
          .endCode
        .endFunction
        .function Register 
          .userFlags 0
          .docString ""
          .return bool
          .paramTable
          .endParamTable
          .localTable
            .local ::temp34 actor
            .local ::temp35 objectreference
            .local ::temp36 bool
            .local ::temp37 bool
            .local ::temp54 var
            .local ::temp38 var
            .local ::temp39 bool
            .local ::temp40 var
            .local ::temp41 bool
            .local ::temp42 var
            .local ::temp43 bool
            .local ::temp44 var
            .local ::temp45 bool
            .local ::temp46 var
            .local ::temp47 bool
            .local ::temp48 var
            .local ::temp49 bool
            .local ::temp50 var
            .local ::temp51 bool
            .local ::temp52 var
            .local ::temp53 bool
          .endLocalTable
          .code
            PROPGET Player self ::temp34 ;@line 86
            CAST ::temp35 ::temp34 ;@line 86
            CALLMETHOD RegisterForAnimationEvent self ::temp36 ::temp35 "BeginWeaponDraw" ;@line 86
            NOT ::temp37 ::temp36 ;@line 86
            JUMPF ::temp37 label23 ;@line 86
            CAST ::temp38 "Failed to register for BeginWeaponDraw Event" ;@line 87
            CALLSTATIC code:log WriteLine ::temp39 Log ::temp38 2 ;@line 87
            JUMP label22
            label23:
            label22:
            PROPGET Player self ::temp34 ;@line 89
            CAST ::temp35 ::temp34 ;@line 89
            CALLMETHOD RegisterForAnimationEvent self ::temp39 ::temp35 "reloadStart" ;@line 89
            NOT ::temp36 ::temp39 ;@line 89
            JUMPF ::temp36 label25 ;@line 89
            CAST ::temp38 "Failed to register for reloadStart Event" ;@line 90
            CALLSTATIC code:log WriteLine ::temp37 Log ::temp38 2 ;@line 90
            JUMP label24
            label25:
            label24:
            PROPGET Player self ::temp34 ;@line 92
            CAST ::temp35 ::temp34 ;@line 92
            CALLMETHOD RegisterForAnimationEvent self ::temp36 ::temp35 "g_weapForceEquipInstant" ;@line 92
            NOT ::temp37 ::temp36 ;@line 92
            JUMPF ::temp37 label27 ;@line 92
            CAST ::temp40 "Failed to register for g_weapForceEquipInstant Event" ;@line 93
            CALLSTATIC code:log WriteLine ::temp41 Log ::temp40 2 ;@line 93
            JUMP label26
            label27:
            label26:
            PROPGET Player self ::temp34 ;@line 95
            CAST ::temp35 ::temp34 ;@line 95
            CALLMETHOD RegisterForAnimationEvent self ::temp41 ::temp35 "g_weaponForceEquipInstant" ;@line 95
            NOT ::temp36 ::temp41 ;@line 95
            JUMPF ::temp36 label29 ;@line 95
            CAST ::temp40 "Failed to register for g_weaponForceEquipInstant Event" ;@line 96
            CALLSTATIC code:log WriteLine ::temp37 Log ::temp40 2 ;@line 96
            JUMP label28
            label29:
            label28:
            PROPGET Player self ::temp34 ;@line 98
            CAST ::temp35 ::temp34 ;@line 98
            CALLMETHOD RegisterForAnimationEvent self ::temp36 ::temp35 "g_archetypeBaseStateStart" ;@line 98
            NOT ::temp37 ::temp36 ;@line 98
            JUMPF ::temp37 label31 ;@line 98
            CAST ::temp42 "Failed to register for g_archetypeBaseStateStart Event" ;@line 99
            CALLSTATIC code:log WriteLine ::temp43 Log ::temp42 2 ;@line 99
            JUMP label30
            label31:
            label30:
            PROPGET Player self ::temp34 ;@line 101
            CAST ::temp35 ::temp34 ;@line 101
            CALLMETHOD RegisterForAnimationEvent self ::temp43 ::temp35 "g_archetypeBaseStateStartInstant" ;@line 101
            NOT ::temp36 ::temp43 ;@line 101
            JUMPF ::temp36 label33 ;@line 101
            CAST ::temp42 "Failed to register for g_archetypeBaseStateStartInstant Event" ;@line 102
            CALLSTATIC code:log WriteLine ::temp37 Log ::temp42 2 ;@line 102
            JUMP label32
            label33:
            label32:
            PROPGET Player self ::temp34 ;@line 104
            CAST ::temp35 ::temp34 ;@line 104
            CALLMETHOD RegisterForAnimationEvent self ::temp36 ::temp35 "weapForceEquip" ;@line 104
            NOT ::temp37 ::temp36 ;@line 104
            JUMPF ::temp37 label35 ;@line 104
            CAST ::temp44 "Failed to register for weapForceEquip Event" ;@line 105
            CALLSTATIC code:log WriteLine ::temp45 Log ::temp44 2 ;@line 105
            JUMP label34
            label35:
            label34:
            PROPGET Player self ::temp34 ;@line 107
            CAST ::temp35 ::temp34 ;@line 107
            CALLMETHOD RegisterForAnimationEvent self ::temp45 ::temp35 "weapEquip" ;@line 107
            NOT ::temp36 ::temp45 ;@line 107
            JUMPF ::temp36 label37 ;@line 107
            CAST ::temp44 "Failed to register for weapEquip Event" ;@line 108
            CALLSTATIC code:log WriteLine ::temp37 Log ::temp44 2 ;@line 108
            JUMP label36
            label37:
            label36:
            PROPGET Player self ::temp34 ;@line 110
            CAST ::temp35 ::temp34 ;@line 110
            CALLMETHOD RegisterForAnimationEvent self ::temp36 ::temp35 "weapUnequip" ;@line 110
            NOT ::temp37 ::temp36 ;@line 110
            JUMPF ::temp37 label39 ;@line 110
            CAST ::temp46 "Failed to register for weapUnequip Event" ;@line 111
            CALLSTATIC code:log WriteLine ::temp47 Log ::temp46 2 ;@line 111
            JUMP label38
            label39:
            label38:
            PROPGET Player self ::temp34 ;@line 113
            CAST ::temp35 ::temp34 ;@line 113
            CALLMETHOD RegisterForAnimationEvent self ::temp47 ::temp35 "weaponAttach" ;@line 113
            NOT ::temp36 ::temp47 ;@line 113
            JUMPF ::temp36 label41 ;@line 113
            CAST ::temp46 "Failed to register for weaponAttach Event" ;@line 114
            CALLSTATIC code:log WriteLine ::temp37 Log ::temp46 2 ;@line 114
            JUMP label40
            label41:
            label40:
            PROPGET Player self ::temp34 ;@line 116
            CAST ::temp35 ::temp34 ;@line 116
            CALLMETHOD RegisterForAnimationEvent self ::temp36 ::temp35 "weaponDetach" ;@line 116
            NOT ::temp37 ::temp36 ;@line 116
            JUMPF ::temp37 label43 ;@line 116
            CAST ::temp48 "Failed to register for weaponDetach Event" ;@line 117
            CALLSTATIC code:log WriteLine ::temp49 Log ::temp48 2 ;@line 117
            JUMP label42
            label43:
            label42:
            PROPGET Player self ::temp34 ;@line 119
            CAST ::temp35 ::temp34 ;@line 119
            CALLMETHOD RegisterForAnimationEvent self ::temp49 ::temp35 "weaponDraw" ;@line 119
            NOT ::temp36 ::temp49 ;@line 119
            JUMPF ::temp36 label45 ;@line 119
            CAST ::temp48 "Failed to register for weaponDraw Event" ;@line 120
            CALLSTATIC code:log WriteLine ::temp37 Log ::temp48 2 ;@line 120
            JUMP label44
            label45:
            label44:
            PROPGET Player self ::temp34 ;@line 122
            CAST ::temp35 ::temp34 ;@line 122
            CALLMETHOD RegisterForAnimationEvent self ::temp36 ::temp35 "weaponSheathe" ;@line 122
            NOT ::temp37 ::temp36 ;@line 122
            JUMPF ::temp37 label47 ;@line 122
            CAST ::temp50 "Failed to register for weaponSheathe Event" ;@line 123
            CALLSTATIC code:log WriteLine ::temp51 Log ::temp50 2 ;@line 123
            JUMP label46
            label47:
            label46:
            PROPGET Player self ::temp34 ;@line 125
            CAST ::temp35 ::temp34 ;@line 125
            CALLMETHOD RegisterForAnimationEvent self ::temp51 ::temp35 "weaponInstantDown" ;@line 125
            NOT ::temp36 ::temp51 ;@line 125
            JUMPF ::temp36 label49 ;@line 125
            CAST ::temp50 "Failed to register for weaponInstantDown Event" ;@line 126
            CALLSTATIC code:log WriteLine ::temp37 Log ::temp50 2 ;@line 126
            JUMP label48
            label49:
            label48:
            PROPGET Player self ::temp34 ;@line 128
            CAST ::temp35 ::temp34 ;@line 128
            CALLMETHOD RegisterForAnimationEvent self ::temp36 ::temp35 "weaponIdle" ;@line 128
            NOT ::temp37 ::temp36 ;@line 128
            JUMPF ::temp37 label51 ;@line 128
            CAST ::temp52 "Failed to register for weaponIdle Event" ;@line 129
            CALLSTATIC code:log WriteLine ::temp53 Log ::temp52 2 ;@line 129
            JUMP label50
            label51:
            label50:
            PROPGET Player self ::temp34 ;@line 131
            CAST ::temp35 ::temp34 ;@line 131
            CALLMETHOD RegisterForAnimationEvent self ::temp53 ::temp35 "unEquip" ;@line 131
            NOT ::temp36 ::temp53 ;@line 131
            JUMPF ::temp36 label53 ;@line 131
            CAST ::temp52 "Failed to register for unEquip Event" ;@line 132
            CALLSTATIC code:log WriteLine ::temp37 Log ::temp52 2 ;@line 132
            JUMP label52
            label53:
            label52:
            CAST ::temp54 "Registered for animEvents." ;@line 135
            CALLSTATIC code:log WriteLine ::temp36 Log ::temp54 0 ;@line 135
            RETURN True ;@line 136
          .endCode
        .endFunction
      .endState
    .endStateTable
  .endObject
.endObjectTable