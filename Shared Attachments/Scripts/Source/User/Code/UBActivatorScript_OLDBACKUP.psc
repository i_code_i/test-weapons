Scriptname Code:UBActivatorScript_OLDBACKUP extends activemagiceffect

Group Properties
	Form Property ActivatorItem Auto
	Keyword Property AnimGL Auto Const Mandatory
	Keyword Property AnimMK Auto Const Mandatory
	Keyword Property ActiveUB Auto Const Mandatory
	ObjectMod Property MasterKey Auto Const Mandatory
	ObjectMod Property GrenadeLauncher Auto Const Mandatory
EndGroup


Actor Player
Weapon CurrentWeapon

Event OnEffectStart(Actor akTarget, Actor akCaster)
	Player = Game.GetPlayer()
	CurrentWeapon = Player.GetEquippedWeapon()
	if (Player.WornHasKeyword(AnimGL) && !Player.WornHasKeyword(ActiveUB))
		Player.AttachModToInventoryItem(CurrentWeapon, GrenadeLauncher)
		Player.AddKeyword(ActiveUB)
		Player.AddItem(ActivatorItem, 1, True)
		debug.Notification("Grenade Launcher Active")
	elseif (player.WornHasKeyword(AnimMK) && !Player.WornHasKeyword(ActiveUB))
		Player.AttachModToInventoryItem(CurrentWeapon, MasterKey)
		Player.AddKeyword(ActiveUB)
		Player.AddItem(ActivatorItem, 1, True)
		debug.Notification("Grenade Launcher Active")
	else
		Player.RemoveModFromInventoryItem(CurrentWeapon, MasterKey)
		Player.RemoveModFromInventoryItem(CurrentWeapon, GrenadeLauncher)
		Player.RemoveKeyword(ActiveUB)
		Player.AddItem(ActivatorItem, 1, True)
	endif
		
EndEvent
