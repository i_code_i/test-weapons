Scriptname Code:DebugFunctions extends ScriptObject hidden

import Code
import Code:Log



bool Function GetOMODsAtSlotIndex(int aSlotIndex) global

	ObjectMod[] objectMods = Game.GetPlayer().GetWornItemMods(aSlotIndex)
	If (objectMods)
		int index = 0
		While (index < objectMods.Length)
            if Debug.TraceUser("Code_Debug", "Found the object mod '" + objectMods[index] + "' on slot index " + aSlotIndex)
			    ;success
            else
                ;no log found. open new log
                Debug.OpenUserLog("Code_Debug")
                Debug.TraceUser("Code_Debug", "Found the object mod '" + objectMods[index] + "' on slot index " + aSlotIndex)
            endif
            index += 1
		EndWhile
		return True
	Else
		if Debug.TraceUser("Code_Debug", "Slot index " + aSlotIndex + " had no object mods.")
            ;success
        else
            ;no log found. open new log
            Debug.OpenUserLog("Code_Debug")
            Debug.TraceUser("Code_Debug", "Slot index " + aSlotIndex + " had no object mods.")
        endif
		return False
	EndIf
	
EndFunction

Function GetAllWornItemInfo() global
	int index = 0
	int end = 43 const
	
	While (index < end)
		Actor:WornItem wornItem = Game.GetPlayer().GetWornItem(index)
		if Debug.TraceUser("Code_Debug", "Slot Index: " + index + ", " + wornItem)
            ;success
        else
            ;no log found. open new log
            Debug.OpenUserLog("Code_Debug")
            Debug.TraceUser("Code_Debug", "Slot Index: " + index + ", " + wornItem)
        endif
		GetOMODsAtSlotIndex(index)
		index += 1
	EndWhile
EndFunction