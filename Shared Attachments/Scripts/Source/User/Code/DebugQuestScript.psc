Scriptname Code:DebugQuestScript extends ReferenceAlias

import Code
import Code:Log

UserLog Log

Group Properties
	Actor Property Player Hidden
		Actor Function Get()
			return Game.GetPlayer()
		EndFunction
	EndProperty
	int Property StateID Hidden
		int Function Get()
			return Player.GetAnimationVariableInt("iSyncWeaponDrawState")
		EndFunction
	EndProperty
	string Property StateIDName Hidden
		string Function Get()
			return GetWeaponDrawState(StateID)
		EndFunction
	EndProperty
EndGroup

Event OnAliasInit()
	Log = new UserLog
	Log.Caller = self
	Log.FileName = "Code_UnderBarrel"
	Register()
	WriteLine(Log, "This alias is initialized")	
EndEvent

Event OnAnimationEvent(ObjectReference akSource, string asEventName)
	WriteLine(Log, akSource+" received anim event: "+asEventName)
	;WriteLine(Log, "Current iSyncWeaponDrawState is "+StateID+" and it's pointing to "+StateIDName)
EndEvent

Event OnPlayerLoadGame()
	Log = GetLog(self)
	Log.FileName = "Code_UnderBarrel"
	Register()
EndEvent

;State Machine #109 "EquipBehavior"
;Uses Variable iSyncWeaponDrawState to choose the StartStateID
; ////////////////////////////////////////
; //			State IDs				//
; // 0  WPNEquipFast					//
; // 1  WPNEquip						//
; // 2  CombatState						//
; // 4  DynamicActivation				//
; // 16 WPNUnEquip						//
; // 17 DynamicActivationAllowMovement	//
; // 18 WPNInstantDown					//
; ////////////////////////////////////////
String Function GetWeaponDrawState(int i)
	if i == 0
		return "WPNEquipFast"
	endif
	if i == 1
		return "WPNEquip"
	endif
	if i == 2
		return "CombatState"
	endif
	if i == 4
		return "DynamicActivation"
	endif
	if i == 16
		return "WPNUnEquip"
	endif
	if i == 17
		return "DynamicActivationAllowMovement"
	endif
	if i == 18
		return "WPNInstantDown"
	endif
	if (i != 0 && i != 1 && i != 2 && i != 4 && i != 16 && i != 17 && i != 18)
		WriteLine(Log, "Could not determine the value of iSyncWeaponDrawState", 2)
		return "Unknown"
	endif
EndFunction

bool Function Register()
	if (!RegisterForAnimationEvent(Player, "BeginWeaponDraw"))
		WriteLine(Log, "Failed to register for BeginWeaponDraw Event", 2)
	Endif
	if (!RegisterForAnimationEvent(Player, "reloadStart"))
		WriteLine(Log, "Failed to register for reloadStart Event", 2)
	Endif
	if (!RegisterForAnimationEvent(Player, "g_weapForceEquipInstant"))
		WriteLine(Log, "Failed to register for g_weapForceEquipInstant Event", 2)
	Endif
	if (!RegisterForAnimationEvent(Player, "g_weaponForceEquipInstant"))
		WriteLine(Log, "Failed to register for g_weaponForceEquipInstant Event", 2)
	Endif
	if (!RegisterForAnimationEvent(Player, "g_archetypeBaseStateStart"))
		WriteLine(Log, "Failed to register for g_archetypeBaseStateStart Event", 2)
	Endif
	if (!RegisterForAnimationEvent(Player, "g_archetypeBaseStateStartInstant"))
		WriteLine(Log, "Failed to register for g_archetypeBaseStateStartInstant Event", 2)
	Endif
	if (!RegisterForAnimationEvent(Player, "weapForceEquip"))
		WriteLine(Log, "Failed to register for weapForceEquip Event", 2)
	Endif
	if (!RegisterForAnimationEvent(Player, "weapEquip"))
		WriteLine(Log, "Failed to register for weapEquip Event", 2)
	Endif
	if (!RegisterForAnimationEvent(Player, "weapUnequip"))
		WriteLine(Log, "Failed to register for weapUnequip Event", 2)
	Endif
	if (!RegisterForAnimationEvent(Player, "weaponAttach"))
		WriteLine(Log, "Failed to register for weaponAttach Event", 2)
	Endif
	if (!RegisterForAnimationEvent(Player, "weaponDetach"))
		WriteLine(Log, "Failed to register for weaponDetach Event", 2)
	Endif
	if (!RegisterForAnimationEvent(Player, "weaponDraw"))
		WriteLine(Log, "Failed to register for weaponDraw Event", 2)
	Endif
	if (!RegisterForAnimationEvent(Player, "weaponSheathe"))
		WriteLine(Log, "Failed to register for weaponSheathe Event", 2)
	Endif
	if (!RegisterForAnimationEvent(Player, "weaponInstantDown"))
		WriteLine(Log, "Failed to register for weaponInstantDown Event", 2)
	Endif
	if (!RegisterForAnimationEvent(Player, "weaponIdle"))
		WriteLine(Log, "Failed to register for weaponIdle Event", 2)
	Endif
	if (!RegisterForAnimationEvent(Player, "unEquip"))
		WriteLine(Log, "Failed to register for unEquip Event", 2)
	Endif

	WriteLine(Log, "Registered for animEvents.")
	return True
EndFunction

