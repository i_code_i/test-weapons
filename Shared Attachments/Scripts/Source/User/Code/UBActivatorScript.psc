Scriptname Code:UBActivatorScript extends activemagiceffect

import Code
import Code:Log

Group Properties
	Form Property ActivatorItem Auto Const
	ObjectMod Property MasterKey Auto
	ObjectMod Property GrenadeLauncher Auto
	Action Property ActionCustomGrenadeLauncherTo Auto Const
	Action Property ActionCustomGrenadeLauncherFrom Auto Const
	Action Property ActionCustomMasterKeyTo Auto Const
	Action Property ActionCustomMasterKeyFrom Auto Const
	Keyword Property AnimsActive Auto Const
	Keyword Property AnimsGL Auto
	Keyword Property AnimsMK01 Auto
EndGroup

Group Vars
	ActorValue Property IsGrenadeLauncher Auto
	ActorValue Property IsMasterKey Auto
	ActorValue Property IsAttaching Auto
	ActorValue Property SpecialEquip Auto
EndGroup

UserLog Log

Event OnEffectStart(Actor akTarget, Actor akCaster)
	Log = GetLog(self)
	Log.FileName = "Code_UnderBarrel"
	Form CurrentWeapon = akTarget.GetEquippedWeapon()
	String CurrentWeaponName = CurrentWeapon.GetName()
	if (akTarget.WornHasKeyword(AnimsGL))
		;GrenadeLauncher State
		if(!akTarget.WornHasKeyword(AnimsActive))
			;Add
			WriteLine(Log, akTarget+" Grenade Launcher is now active on their "+CurrentWeapon+" AKA: "+CurrentWeaponName)
			akTarget.AddItem(ActivatorItem, 1, True)
			;akTarget.SetValue(SpecialEquip, 1.0)
			
			akTarget.AttachModToInventoryItem(CurrentWeapon, GrenadeLauncher)
			;akTarget.PlaySubGraphAnimation("00NextClip")
			;akTarget.PlayAnimation("00NextClip")

			;We need to wait for 3D before playing the animation
			(akTarget as ObjectReference).WaitFor3DLoad()

			akTarget.PlayIdleAction(ActionCustomGrenadeLauncherTo)
			;Utility.Wait(5.0)
			;akTarget.SetValue(SpecialEquip, 0.0)
		elseif(akTarget.WornHasKeyword(AnimsActive))
			;Remove
			WriteLine(Log, akTarget+" Grenade Launcher is now inactive on their "+CurrentWeapon+" AKA: "+CurrentWeaponName)
			akTarget.AddItem(ActivatorItem, 1, True)
			;akTarget.SetValue(SpecialEquip, 1.0)

			akTarget.RemoveModFromInventoryItem(CurrentWeapon, GrenadeLauncher)
			;akTarget.PlaySubGraphAnimation("00NextClip")
			;akTarget.PlayAnimation("00NextClip")

			;We need to wait for 3D before playing the animation
			(akTarget as ObjectReference).WaitFor3DLoad()

			akTarget.PlayIdleAction(ActionCustomGrenadeLauncherFrom)
			;Utility.Wait(5.0)
			;akTarget.SetValue(SpecialEquip, 0.0)
		else
			;Do Nothing
			WriteLine(Log, "No underbarrel attachments found.", 2)
			akTarget.AddItem(ActivatorItem, 1, True)
		endif
	elseif (akTarget.WornHasKeyword(AnimsMK01))
		;Masterkey State
		if(!akTarget.WornHasKeyword(AnimsActive))
			;Add
			WriteLine(Log, akTarget+" MasterKey is now active on their "+CurrentWeapon+" AKA: "+CurrentWeaponName)
			akTarget.AddItem(ActivatorItem, 1, True)
			;akTarget.SetValue(SpecialEquip, 1.0)
			
			akTarget.AttachModToInventoryItem(CurrentWeapon, MasterKey)
			
			;We need to wait for 3D before playing the animation
			(akTarget as ObjectReference).WaitFor3DLoad()

			akTarget.PlayIdleAction(ActionCustomMasterKeyTo)
			;Utility.Wait(5.0)
			;akTarget.SetValue(SpecialEquip, 0.0)
		elseif(akTarget.WornHasKeyword(AnimsActive))
			;Remove
			WriteLine(Log, akTarget+" MasterKey is now inactive on their "+CurrentWeapon+" AKA: "+CurrentWeaponName)
			akTarget.AddItem(ActivatorItem, 1, True)
			;akTarget.SetValue(SpecialEquip, 1.0)
			
			akTarget.RemoveModFromInventoryItem(CurrentWeapon, MasterKey)
			
			;We need to wait for 3D before playing the animation
			(akTarget as ObjectReference).WaitFor3DLoad()

			akTarget.PlayIdleAction(ActionCustomMasterKeyFrom)
			;Utility.Wait(5.0)
			;akTarget.SetValue(SpecialEquip, 0.0)
		else
			;Do Nothing
			WriteLine(Log, "No underbarrel attachments found.", 2)
			akTarget.AddItem(ActivatorItem, 1, True)
		endif
	else
		;Do Nothing
		WriteLine(Log, "No underbarrel attachments found.", 2)
		akTarget.AddItem(ActivatorItem, 1, True)
	endif
		
EndEvent

Event OnEffectFinish(Actor akTarget, Actor akCaster)
	akTarget.SetValue(SpecialEquip, 0.0)
	;Debug.Notification("")
EndEvent
