Scriptname Code:ModSlotsScript extends Quest

import Code
import Code:Log

Group Properties
    FormList Property do_ModMenuSlotKeywordList Auto
    FormList Property Code_ModMenuSlotKeywordList Auto
    Quest Property Code_Quest_ModSlots Auto
EndGroup

Event OnQuestInit()
    AddKeyordsToFormlist(Code_ModMenuSlotKeywordList, do_ModMenuSlotKeywordList)
	Code_Quest_ModSlots.Stop()
EndEvent

Function AddKeyordsToFormlist(Formlist KeywordToAddList, FormList KeywordList)
    int i = 0
    while (i < KeywordToAddList.GetSize())
        Form KeywordToAdd = KeywordToAddList.GetAt(i) as Form
        KeywordList.AddForm(KeywordToAdd)

        i += 1
    endwhile
EndFunction
