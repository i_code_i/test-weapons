Scriptname Code:ThermalScopeScript extends ActiveMagicEffect

RefCollectionAlias Property ReconTargets Auto Const
float Property ThermalMarkerTime = 1.0 Auto Const
EffectShader Property ThermalFXS Auto Const

Actor TargetRef

Event OnEffectStart(Actor akTarget, Actor akCastor)
	TargetRef = akTarget
	ReconTargets.AddRef(TargetRef)
	;StartTimer(ThermalMarkerTime, 6565)
	ThermalFXS.Play(TargetRef, ThermalMarkerTime)
EndEvent

; Event OnTimer(Int aiTimerID)
; 	If aiTimerID == 6565
; 		ReconTargets.RemoveRef(TargetRef)
;		ThermalFXS.Stop(TargetRef)
; 	endif
; EndEvent

Event OnEffectFinish(Actor akTarget, Actor akCaster)
	;ThermalFXS.Stop(akTarget)
EndEvent

;REMINDER: Can't do remove marker on effect finish because recasts stomp each other
;Event OnEffectFinish(Actor akTarget, Actor akCaster)
	;ReconTargets.RemoveRef(TargetRef)
;EndEvent