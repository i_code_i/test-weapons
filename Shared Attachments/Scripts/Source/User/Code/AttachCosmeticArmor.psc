Scriptname Code:AttachCosmeticArmor extends ActiveMagicEffect
{Attach Armor to actor}

Armor Property CharmArmor Auto Const
{armor to equip}


Event OnEffectStart(Actor akTarget, Actor akCaster)
    akCaster.EquipItem(CharmArmor, False, True)
EndEvent

Event OnEffectFinish(Actor akTarget, Actor akCaster)
	akCaster.RemoveItem(CharmArmor, 1, True)
EndEvent