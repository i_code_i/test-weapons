# Test Weapons

This is a repository to share my WIP weapons and frameworks for Fallout 4

## MW Ports

Folder for MW ports

### PKM

MW 2019 PKM Port

### SCAR

MW 2019 SCAR Port

## Extended Weapon Skeleton

An extended skeleton designed to add more bones to weapons

## Extended Weapon Systems

A framework to add more features to the weapon behavior

## Extended Weapon Systems Patches

Patches for other authors weapon mods

## Shared Attachments

A collection of attachments from MW 2019 and other sources

## Stoner63A

Killing Floor 2 Stoner63A Port

## Test Wep Data

Weapons that I am testing

## License

Please do not reupload these mods or any of this assets without permission