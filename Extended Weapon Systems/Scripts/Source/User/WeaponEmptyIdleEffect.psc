ScriptName WeaponEmptyIdleEffect extends ActiveMagicEffect
{  }

;-- Properties --------------------------------------
Group Main
	ActorValue Property WeaponStatusAV Auto Const
EndGroup


;-- Variables ---------------------------------------


;-- Functions ---------------------------------------

Function Trace(string asTextToPrint, int aiSeverity)
	string debugText = "WeaponEmptyIdleEffect : " + Self as string + " : " + asTextToPrint
	Debug.Trace(debugText, aiSeverity)
EndFunction

Event OnEffectStart(Actor akTarget, Actor akCaster)
	Game.GetPlayer().SetValue(WeaponStatusAV, 1)
	;Game.GetPlayer().SetAnimationVariableFloat("fWeaponEmpty", 1.0)
	RegisterForAnimationEvent(Game.GetPlayer(), "ReloadStart")
	RegisterForAnimationEvent(Game.GetPlayer(), "ReloadSpeedStart")
	RegisterForAnimationEvent(Game.GetPlayer(), "ReloadEmptyStart")
	RegisterForAnimationEvent(Game.GetPlayer(), "ReloadEmptySpeedStart")
EndEvent

Event OnAnimationEvent(ObjectReference akSource, string asEventName)
	if (akSource == Game.GetPlayer()) && (asEventName == "ReloadStart") || (asEventName == "ReloadSpeedStart") || (asEventName == "ReloadEmptyStart") || (asEventName == "ReloadEmptySpeedStart")
		Game.GetPlayer().SetValue(WeaponStatusAV, 0)
		Game.GetPlayer().SetAnimationVariableFloat("fWeaponEmpty", 0.99)
	endIf
EndEvent

Event OnEffectFinish(Actor akTarget, Actor akCaster)
	UnregisterForAnimationEvent(Game.GetPlayer(), "ReloadEnd")
	UnregisterForAnimationEvent(Game.GetPlayer(), "ReloadSpeedStart")
	UnregisterForAnimationEvent(Game.GetPlayer(), "ReloadEmptyStart")
	UnregisterForAnimationEvent(Game.GetPlayer(), "ReloadEmptySpeedStart")
	Game.GetPlayer().SetValue(WeaponStatusAV, 0)
	Game.GetPlayer().SetAnimationVariableFloat("fWeaponEmpty", 0.99)
EndEvent
