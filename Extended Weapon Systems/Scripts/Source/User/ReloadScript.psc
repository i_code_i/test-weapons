Scriptname ReloadScript extends ActiveMagicEffect

ActorValue Property ChamberAV Auto Const

Event OnEffectStart(Actor akTarget, Actor akCaster)
	Actor Player = Game.GetPlayer()
	Player.SetValue(ChamberAV, 1)
	; Player.PlaySubGraphAnimation("reloadEmptyStart")
	RegisterForAnimationEvent(Player, "ReloadEnd")
EndEvent

Event OnAnimationEvent(ObjectReference akSource, string asEventName)
	if (akSource == Game.GetPlayer()) && (asEventName == "ReloadEnd")
		Game.GetPlayer().SetValue(ChamberAV, 0)
		UnregisterForAnimationEvent(Game.GetPlayer(), "ReloadEnd")
	endIf
EndEvent
