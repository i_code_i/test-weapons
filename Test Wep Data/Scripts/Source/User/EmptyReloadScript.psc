Scriptname EmptyReloadScript extends ActiveMagicEffect

ActorValue Property ReloadAV Auto Const

Event OnEffectStart(Actor akTarget, Actor akCaster)
	Actor Player = Game.GetPlayer()
	Player.SetValue(ReloadAV, 1)
	; Player.PlaySubGraphAnimation("reloadEmptyStart")
	RegisterForAnimationEvent(Player, "ReloadEnd")
EndEvent

Event OnAnimationEvent(ObjectReference akSource, string asEventName)
	if (akSource == Game.GetPlayer()) && (asEventName == "ReloadEnd")
		Game.GetPlayer().SetValue(ReloadAV, 0)
		UnregisterForAnimationEvent(Game.GetPlayer(), "ReloadEnd")
	endIf
EndEvent
