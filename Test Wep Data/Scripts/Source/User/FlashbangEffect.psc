ScriptName FlashbangEffect extends ActiveMagicEffect default
{ Plays a sound on the affected client while this MagicEffect is active }

;-- Properties --------------------------------------
Group Main
	Sound Property SoundToPlay Auto Const mandatory
	{ Sound to play when this effect is active }
	soundcategorysnapshot Property SnapshotToPlay Auto Const
	{ Optional snapshot to play when this effect is active }
EndGroup


;-- Variables ---------------------------------------
int soundToPlayID = -1

;-- Functions ---------------------------------------

Function Trace(string asTextToPrint, int aiSeverity)
	string debugText = "DefaultEffectPlaySound : " + Self as string + " : " + asTextToPrint
	Debug.Trace(debugText, aiSeverity)
EndFunction

Event OnEffectFinish(Actor akTarget, Actor akCaster)
	If (soundToPlayID >= 0)
		Sound.StopInstance(soundToPlayID)
		soundToPlayID = -1
	EndIf
	If (SnapshotToPlay != None)
		SnapshotToPlay.Remove()
	EndIf
EndEvent

Event OnEffectStart(Actor akTarget, Actor akCaster)
	soundToPlayID = SoundToPlay.Play(akTarget as ObjectReference)
	If (SnapshotToPlay != None)
		SnapshotToPlay.Push(1)
	EndIf
EndEvent
